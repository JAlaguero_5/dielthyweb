describe('Comments in a topic', () => {

  it('Add comment', () => {

    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('javi6798@hotmail.es');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(10000);

    cy.visit('/forum');
    cy.wait(5000);

    cy.get('[data-test="messages-number"]').first().invoke('text').then(size => {
      cy.get('[data-test="topic-name"]').first().click();
      cy.wait(8000);
      cy.get('[data-test="add-comment-button"]').click();
      cy.get('[data-test="comment-input"]').type('Prueba desde cypress');
      cy.get('[data-test="save-comment-button"]').click();
      cy.wait(10000);
      cy.visit('/forum');
      cy.wait(8000);
      cy.get('[data-test="messages-number"]').first().invoke('text').then(size2 => {
        expect(parseInt(size2)).to.equal(parseInt(size) + 1);
      });
    });

  });

  it('Add answer', () => {
    cy.visit('/forum');
    cy.wait(5000);

    cy.get('[data-test="messages-number"]').first().invoke('text').then(size => {
      cy.get('[data-test="topic-name"]').first().click();
      cy.wait(8000);
      cy.get('[data-test="reply-button"]').first().click();
      cy.get('[data-test="comment-input"]').type('Respuesta desde cypress');
      cy.get('[data-test="save-comment-button"]').click();
      cy.wait(10000);
      cy.visit('/forum');
      cy.wait(8000);
      cy.get('[data-test="messages-number"]').first().invoke('text').then(size2 => {
        expect(parseInt(size2)).to.equal(parseInt(size) + 1);
      });
    });

  });

  it('Comments equals inside and outside a topic', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(10000);

    cy.visit('/forum');
    cy.wait(10000);

    cy.get('[data-test="messages-number"]').first().invoke('text').then(size => {
      cy.get('[data-test="topic-name"]').first().click();
      cy.wait(7000);
      cy.get('[data-test="comment"]').its('length').then(size2 => {
        cy.get('[data-test="answer"]').its('length').then(size3 => {
          expect(parseInt(size)).to.equal(size2 + size3);
        });
      });
    });
    
  });

  it('Add comment without text', () => {
      cy.visit('/forum');
      cy.wait(7000);
      cy.get('[data-test="topic-name"]').first().click();
      cy.wait(8000);
      cy.get('[data-test="add-comment-button"]').click();
      cy.get('[data-test="save-comment-button"]').click();
      cy.get('.v-input').should('have.class', 'error--text');
  });

});