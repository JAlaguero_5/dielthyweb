describe('Report comment', () => {
  it('Report comment correctly', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('javi6798@hotmail.es');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(10000);

    cy.visit('/forum');
    cy.wait(8000);
    cy.get('[data-test="topic-name"]').first().click();
    cy.wait(8000);
    cy.get('[data-test="add-comment-button"]').click();
    cy.get('[data-test="comment-input"]').type('Prueba desde cypress');
    cy.get('[data-test="save-comment-button"]').click();
    cy.wait(10000);
    cy.get('[data-test="logout-text"]').first().click();
    cy.get('[data-test="confirm-button"]').click();
    cy.wait(2000);

    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(10000);
    cy.visit('/forum');
    cy.wait(8000);
    cy.get('[data-test="topic-name"]').first().click();
    cy.wait(8000);

    cy.get('[data-test="report-button"]').first().click();
    cy.get('[data-test="confirm-button"]').click();
    cy.wait(3000);
    cy.get('[data-test="reported-comment"]').should('be.visible');
  });
});