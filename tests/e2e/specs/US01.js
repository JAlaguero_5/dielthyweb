describe('Search aditive', () => {
  it('Correct aditive', () => {
    cy.visit('/');
    cy.get('[data-test="aditive-code"]').type('200');
    cy.get('[data-test="search-button"]').click();
    cy.location('pathname').should('eq', '/aditive');
    cy.get('[data-test="code"]').contains('E-200');
    cy.get('[data-test="description"]').contains('Ácido sórbico');
    cy.get('[data-test="icon-sprout"]').should('not.exist');
    cy.get('[data-test="icon-flask"]').should('exist');
    cy.get('[data-test="icon-pig"]').should('not.exist');
    cy.get('[data-test="icon-help"]').should('not.exist');
    cy.get('[data-test="type"]').contains('Conservante');
    cy.get('[data-test="icon-happy-veg"]').should('exist');
    cy.get('[data-test="icon-sad-veg"]').should('not.exist');
    cy.get('[data-test="icon-happy-cel"]').should('exist');
    cy.get('[data-test="icon-sad-cel"]').should('not.exist');
    cy.get('[data-test="icon-happy-lac"]').should('exist');
    cy.get('[data-test="icon-sad-lac"]').should('not.exist');
  });

  it('Search empty code', () => {
    cy.visit('/');
    cy.get('[data-test="search-button"]').click();
    cy.location('pathname').should('eq', '/');
    cy.get('[data-test="aditive-code"]').its('error-messages').should('be', 'Se debe introducir un código');
  });

  it('Search empty code and write after', () => {
    cy.visit('/');
    cy.get('[data-test="search-button"]').click();
    cy.location('pathname').should('eq', '/');
    cy.get('[data-test="aditive-code"]').its('error-messages').should('be', 'Se debe introducir un código');
    cy.get('[data-test="aditive-code"]').type('1');
    cy.get('[data-test="aditive-code"]').its('error-messages').should('be.empty');
  });

  it('Returning to home', () => {
    cy.visit('/');
    cy.get('[data-test="aditive-code"]').type('200');
    cy.get('[data-test="search-button"]').click();
    cy.get('[data-test="button-main"]').click();
    cy.location('pathname').should('eq', '/');
  });

  it('Redirect to home when reloading aditive page', () => {
    cy.visit('/aditive');
    cy.location('pathname').should('eq', '/');
  });

  it('Aditive not found', () => {
    cy.visit('/');
    cy.get('[data-test="aditive-code"]').type('105');
    cy.get('[data-test="search-button"]').click();
    cy.location('pathname').should('eq', '/');
    cy.get('[data-test="aditive-code"]').its('error-messages').should('be', 'No existe ningún aditivo con ese código');
  });

});
