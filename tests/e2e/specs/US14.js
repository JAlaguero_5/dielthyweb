describe('Delete user own comment', () => {
  it('Delete comment user', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(10000);

    cy.visit('/forum');
    cy.wait(5000);

    cy.get('[data-test="topic-name"]').first().click();
    cy.wait(8000);
    cy.get('[data-test="add-comment-button"]').click();
    cy.get('[data-test="comment-input"]').type('Prueba desde cypress eliminar comentario');
    cy.get('[data-test="save-comment-button"]').click();
    cy.wait(10000);

    cy.get('[data-test="delete-comment"]').first().click();
    cy.get('[data-test="confirm-button"]').click();
    cy.wait(3000);
    cy.get('[data-test="deleted-comment"]').should('be.visible');
  });
});