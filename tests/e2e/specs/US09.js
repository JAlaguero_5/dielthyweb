describe('Add topic', () => {
  
  it('Name empty', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(10000);

    cy.visit('/forum');
    cy.wait(5000);

    cy.get('[data-test="add-topic-button"]').first().click();
    cy.get('[data-test="add-topic-tag"]').parent().click();
    cy.get('.v-list-item').first().click();
    cy.get('.dialog-add-topic').get('.v-input__icon--append')
    cy.get('.dialog-add-topic').get('.v-input__icon--append').eq(4).click();
    cy.get('[data-test="save-topic-button"]').click();
    cy.get('.v-input').eq(5).should('have.class', 'error--text');
  });

  it('Tag empty', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(5000);

    cy.visit('/forum');
    cy.wait(5000);

    cy.get('[data-test="add-topic-button"]').first().click();
    cy.get('[data-test="add-topic-name"]').type('Prueba cypress');
    cy.get('[data-test="add-topic-lang"]').parent().click();
    cy.get('.v-list-item').first().click();
    cy.get('[data-test="save-topic-button"]').click();
    cy.get('.v-input').eq(6).should('have.class', 'error--text');
  });

  it('Lang empty', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(5000);

    cy.visit('/forum');
    cy.wait(5000);

    cy.get('[data-test="add-topic-button"]').first().click();
    cy.get('[data-test="add-topic-name"]').type('Prueba cypress');
    cy.get('[data-test="add-topic-tag"]').parent().click();
    cy.get('.v-list-item').first().click();
    cy.get('.dialog-add-topic').get('.v-input__icon--append').eq(4).click();
    cy.get('[data-test="save-topic-button"]').click();
    cy.get('.v-input').eq(7).should('have.class', 'error--text');
  });


  it('Adding correct topic', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(5000);

    cy.visit('/forum');
    cy.wait(5000);

    cy.get('[data-test="add-topic-button"]').first().click();
    cy.get('[data-test="add-topic-name"]').type('Prueba cypress');
    cy.get('[data-test="add-topic-tag"]').parent().click();
    cy.get('.v-list-item').first().click();
    cy.get('.dialog-add-topic').get('.v-input__icon--append').eq(4).click();
    cy.get('[data-test="add-topic-lang"]').parent().click();
    cy.get('.v-list-item').eq(4).click();
    cy.get('[data-test="save-topic-button"]').click();
    cy.wait(2000);
    cy.get('[data-test="saved-topic"]').should('be.visible');
  });

});