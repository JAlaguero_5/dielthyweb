describe('Delete user', () => {
  it('Delete user correctly', () => {

    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero.nscar@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(10000);

    cy.visit('/forum');
    cy.wait(5000);
    cy.get('[data-test="topic-name"]').first().click();
    cy.wait(8000);
    cy.get('[data-test="add-comment-button"]').click();
    cy.get('[data-test="comment-input"]').type('Prueba desde cypress borrar usuario');
    cy.get('[data-test="save-comment-button"]').click();
    cy.wait(10000);

    cy.get('[data-test="logout-text"]').click();
    cy.get('[data-test="confirm-button"]').click();

    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(10000);
    cy.visit('/forum');
    cy.wait(5000);
    cy.get('[data-test="topic-name"]').first().click();
    cy.wait(8000);
    cy.get('.info-general__user').first().click();
    cy.wait(5000);

    cy.get('[data-test="delete-button"]').click();
    cy.get('[data-test="confirm-button"]').click();
    cy.wait(2000);
    cy.get('[data-test="deleted-success"]').should('be.visible');
    cy.wait(2000);
    cy.location('pathname').should('eq', '/');

  });
});