describe('Reports page', () => {

  it('Normal user cannot access but admin yes', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('javi6798@hotmail.es');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(10000);
    cy.visit('/reports');
    cy.wait(2000);
    cy.location('pathname').should('eq', '/');

    cy.get('[data-test="logout-text"]').first().click();
    cy.get('[data-test="confirm-button"]').click();
    cy.wait(2000);
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(10000);
    cy.visit('/reports');
    cy.location('pathname').should('eq', '/reports');
  });

  it('Go to user profile when click on user comment', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(10000);
    cy.visit('/reports');
    cy.wait(8000);
    cy.get('[data-test="comment-user"]').first().click();
    cy.location('pathname').should('to.contain', '/profile');
  });

  it('Go to user profile when click on user report', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(10000);
    cy.visit('/reports');
    cy.wait(8000);
    cy.get('[data-test="number-users"]').first().click();
    cy.get('[data-test="username-report"]').first().click();
    cy.location('pathname').should('to.contain', '/profile');
  });

});