describe('Main page forum', () => {

  it('Not allowing to show forum without login', () => {
    cy.visit('/');
    cy.get('[data-test="button-forum"]').click();
    cy.location('pathname').should('eq', '/login');
  });

  it('Allow to visit forum with login', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(5000);
    cy.get('[data-test="button-forum"]').click();
    cy.location('pathname').should('eq', '/forum');
  });

  it('Go to user profile when click on a user', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(5000);
    cy.get('[data-test="button-forum"]').click();
    cy.get('.created__user').first().click();
    cy.location('pathname').should('to.contain', '/profile')
  });

});
