describe('Login user', () => {

  it('Correct login', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(5000);
    cy.location('pathname').should('eq', '/');
    cy.get('[data-test="username-text"]').should('be.visible');
  });

  it('Correct login and logout', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(5000);
    cy.location('pathname').should('eq', '/');
    cy.get('[data-test="logout-text"]').first().click();
    cy.get('[data-test="confirm-button"]').click();
    cy.get('[data-test="login-text"]').should('be.visible');
  });

  it('Login fails due to email', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('ejemplo@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.get('[data-test="login-text"]').should('be.visible');
    cy.get('[data-test="show-error"]').should('be.visible');
  });

  it('Login fails due to password', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('hola');
    cy.get('[data-test="login-button"]').click();
    cy.get('[data-test="login-text"]').should('be.visible');
    cy.wait(5000);
    cy.get('[data-test="show-error"]').should('be.visible');
  });

});

describe('Password forgotten', () => {
  it('Send email correctly', () => {
    cy.visit('/login');
    cy.get('[data-test="password-forgotten"]').click();
    cy.get('[data-test="email-reset-password"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="send-email-button"]').click();
    cy.get('[data-test="email-reset-password"]').should('not.be.visible');
    cy.wait(5000);
    cy.get('[data-test="send-email-ok"]').should('be.visible');
  });

  it('Send email fails', () => {
    cy.visit('/login');
    cy.get('[data-test="password-forgotten"]').click();
    cy.get('[data-test="email-reset-password"]').type('jalaguero@gmail.com');
    cy.get('[data-test="send-email-button"]').click();
    cy.get('[data-test="email-reset-password"]').should('not.be.visible');
    cy.wait(3500);
    cy.get('[data-test="send-email-not-found"]').should('be.visible');
  });
})