describe('User register', () => {
  it('Correct register', () => {
    cy.visit('/register');
    cy.get('[data-test="email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password"]').type('password');
    cy.get('[data-test="password2"]').type('password');
    cy.get('[data-test="username"]').type('jalaguero');
    cy.get('[data-test="switch"]').parent().click();
    cy.get('[data-test="register-button"]').click();
    cy.wait(10000);
    cy.location('pathname').should('eq', '/');
  });

  it('Privacity policy', () => {
    cy.visit('/register');
    cy.get('[data-test="email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password"]').type('password');
    cy.get('[data-test="password2"]').type('password');
    cy.get('[data-test="username"]').type('jalaguero');
    cy.get('[data-test="name"]').type('Javier')
    cy.get('[data-test="switch"]').parent().click();
    cy.get('[data-test="birthDate"]').click();
    cy.get('li').eq(1).click();
    cy.get('.v-date-picker-table').first().get('td').first().click();
    cy.get('td').eq(15).click();
    cy.get('[data-test="ok-button"]').click();
    cy.get('[data-test="birthDate"]').invoke('val').then(text => {
      cy.get('[data-test="checkbox-lac"]').parent().click();
      cy.get('[data-test="privacity-link"]').click();
      cy.location('pathname').should('eq', '/privacity');
      cy.get('[data-test="back-to-register"]').click();
      cy.location('pathname').should('eq', '/register');
      cy.get('[data-test="email"]').should('have.value', 'jalaguero5@gmail.com');
      cy.get('[data-test="password"]').should('have.value', 'password');
      cy.get('[data-test="password2"]').should('have.value', 'password');
      cy.get('[data-test="username"]').should('have.value', 'jalaguero');
      cy.get('[data-test="name"]').should('have.value', 'Javier');
      cy.get('[data-test="birthDate"]').should('have.value', text);
      cy.get('[data-test="checkbox-lac"]').should('be.checked');
    });
  });

});