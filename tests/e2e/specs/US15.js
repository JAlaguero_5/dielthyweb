describe('Dismiss reports comment', () => {

  it('Report a comment and dismiss it', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(10000);

    cy.visit('/forum');
    cy.wait(8000);
    cy.get('[data-test="topic-name"]').first().click();
    cy.wait(6000);

    cy.get('[data-test="report-button"]').first().click();
    cy.get('[data-test="confirm-button"]').click();
    cy.wait(3000);

    cy.visit('/reports');
    cy.wait(5000);
    cy.get('[data-test="dismiss-comment"]').first().click();
    cy.get('[data-test="confirm-button"]').click();
    cy.wait(2500);
    cy.get('[data-test="dismissed-report"]').should('be.visible');
  });

});