describe('User profile', () => {

  it('Show profile correctly', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(5000);
    cy.location('pathname').should('eq', '/');
    cy.get('[data-test="show-profile-text"]').first().click();
    cy.location('pathname').should('to.contain', '/profile');
    cy.get('[data-test="verified-subheader"]').should('exist');
    cy.get('[data-test="show-profile-username"]').should('exist');
    cy.get('[data-test="show-profile-username"]').contains('jalaguero');
    cy.get('[data-test="name-show-profile"]').contains('Sin especificar');
    cy.get('[data-test="dietTypes-show-profile0"]').contains('Ninguno');
    cy.get('[data-test="birthDate-show-profile"]').contains('Sin especificar');
    cy.get('[data-test="email-show-profile"]').contains('jalaguero5@gmail.com');
  });

  it('Edit info', () => {
    cy.visit('/login');
    cy.get('[data-test="login-email"]').type('jalaguero5@gmail.com');
    cy.get('[data-test="password-login"]').type('password');
    cy.get('[data-test="login-button"]').click();
    cy.wait(5000);
    cy.location('pathname').should('eq', '/');
    cy.get('[data-test="show-profile-text"]').first().click();
    cy.location('pathname').should('to.contain', '/profile');
    cy.get('[data-test="edit-button"]').click();
    cy.get('[data-test="verified-subheader"]').should('exist');
    cy.get('[data-test="show-profile-username"]').should('not.exist');
    cy.get('[data-test="name-edit-profile"]').should('have.value', '');
    cy.get('[data-test="birthDate-edit-profile"]').should('have.value', '');
    cy.get('[data-test="email-edit-profile"]').should('have.value', 'jalaguero5@gmail.com');
    cy.get('[data-test="username-edit-profile"]').should('have.value', 'jalaguero');

    cy.get('[data-test="name-edit-profile"]').type('Javier');
    cy.get('[data-test="save-button"]').click();
    cy.wait(10000);
    cy.get('[data-test="show-profile-username"]').should('exist');
    cy.get('[data-test="name-show-profile"]').contains('Javier');
  });


  
});