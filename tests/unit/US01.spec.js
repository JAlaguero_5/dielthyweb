import { mount } from '@vue/test-utils';
import Main from '@/views/Main';
import AditiveInfo from '@/views/AditiveInfo';

import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify'; 
import { i18n } from '../../src/lang/i18n';

Vue.use(Vuetify);
Vue.use(Vuex);


describe('Main.vue', () => {

  let vuetify;
  let wrapper;
  let actions;

  beforeEach(() => {

    actions = {
      loadAditiveInfo: jest.fn()
    };

    vuetify = new Vuetify();
    const store = new Vuex.Store({
      modules: {
        aditive: {
          actions: actions
        }
      }
    });
    wrapper = mount(Main, {
      mocks: {
        $store: store
      },
      vuetify,
      i18n
    });
  });

  it('Show welcome message', ()  => {
    expect(wrapper.find('.main-content__central-text--welcome').text()).toEqual('¡Bienvenido a Dielthy!');
    expect(wrapper.find('.main-content__central-text--info').text()).toEqual('Consulta aquí el aditivo que desees');
  });

  it('Show E- when text is added to the input', async () => {
    const input = wrapper.find('[data-test="aditive-code"]');
    await wrapper.setData({ aditiveNumber: '' });
    await Vue.nextTick();
    await input.setValue('1');
    await Vue.nextTick();
    expect(wrapper.vm.aditiveNumber).toBe('E-1');
    expect(wrapper.vm.errorMessages).toBeNull();
  });

  it('Update empty text when deleting every character except E-', async () => {
    await wrapper.setData({ aditiveNumber: 'E-2' });
    await Vue.nextTick();
    const input = wrapper.find('[data-test="aditive-code"]');
    await input.setValue('E-');
    await Vue.nextTick();
    expect(wrapper.vm.aditiveNumber).toBe('');
  });

  it('Show error message when click on the button with empty text', async () => {
    await wrapper.setData({ aditiveNumber: '' });
    await Vue.nextTick();
    const button = wrapper.find('[data-test="search-button"]');
    await button.trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.errorMessages).toBe('Se debe introducir un código');
  });

  it('Call action to load aditive data', async () => {
    await wrapper.setData({ aditiveNumber: 'E-100' });
    await Vue.nextTick();
    const button = wrapper.find('[data-test="search-button"]');
    await button.trigger('click');
    await Vue.nextTick();
    expect(actions.loadAditiveInfo.mock.calls.length).toBe(1);
    expect(actions.loadAditiveInfo.mock.calls[0][1]).toBe('100');
  });
});

describe('AditiveInfo.vue', () => {

  let vuetify;
  let wrapper;
  let state;

  beforeEach(() => {

    state = {
      aditive: {
        cel: true,
        code: '100',
        description: 'Curcumina',
        lac: true,
        origin: 'Vegetal',
        type: 'Colorante',
        veg: true
      }
    };

    const store = new Vuex.Store({
      modules: {
        aditive: {
          state: state
        },
        user: {
          state: {
            userInfo: {
              dietTypes: ['veg']
            }
          }
        }
      }
    });

    vuetify = new Vuetify();
    wrapper = mount(AditiveInfo, {
      mocks: {
        $store: store
      },
      vuetify,
      i18n
    });
  });

  it('Render component correctly', () => {
    expect(wrapper.find('[data-test="code"]').text()).toBe('E-100');
    expect(wrapper.find('[data-test="description"]').text()).toBe('Curcumina');
    expect(wrapper.find('[data-test="icon-sprout"]').exists()).toBe(true);
    expect(wrapper.find('[data-test="icon-pig"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-help"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-flask"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="type"]').text()).toBe('Colorante');
    expect(wrapper.find('[data-test="icon-happy-veg"]').exists()).toBe(true);
    expect(wrapper.find('[data-test="icon-sad-veg"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-happy-cel"]').exists()).toBe(true);
    expect(wrapper.find('[data-test="icon-sad-cel"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-happy-lac"]').exists()).toBe(true);
    expect(wrapper.find('[data-test="icon-sad-lac"]').exists()).toBe(false);
  });

  it('Render component animal origin correctly', async () => {
    wrapper.vm.$store.state.aditive.aditive.origin = 'Animal';
    await Vue.nextTick();
    expect(wrapper.find('[data-test="icon-sprout"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-pig"]').exists()).toBe(true);
    expect(wrapper.find('[data-test="icon-help"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-flask"]').exists()).toBe(false);
  });

  it('Render component indeterminate origin correctly', async () => {
    wrapper.vm.$store.state.aditive.aditive.origin = 'Indeterminado';
    await Vue.nextTick();
    expect(wrapper.find('[data-test="icon-sprout"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-pig"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-help"]').exists()).toBe(true);
    expect(wrapper.find('[data-test="icon-flask"]').exists()).toBe(false);
  });

  it('Render component chemist origin correctly', async () => {
    wrapper.vm.$store.state.aditive.aditive.origin = 'Químico o sintético';
    await Vue.nextTick();
    expect(wrapper.find('[data-test="icon-sprout"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-pig"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-help"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-flask"]').exists()).toBe(true);
  });

  it('Render component lac false', async () => {
    wrapper.vm.$store.state.aditive.aditive.lac = false;
    await Vue.nextTick();
    expect(wrapper.find('[data-test="icon-happy-lac"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-sad-lac"]').exists()).toBe(true);
  });

  it('Render component veg false', async () => {
    wrapper.vm.$store.state.aditive.aditive.veg = false;
    await Vue.nextTick();
    expect(wrapper.find('[data-test="icon-happy-veg"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-sad-veg"]').exists()).toBe(true);
  });

  it('Render component cel false', async () => {
    wrapper.vm.$store.state.aditive.aditive.cel = false;
    await Vue.nextTick();
    expect(wrapper.find('[data-test="icon-happy-cel"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-sad-cel"]').exists()).toBe(true);
  });

  it('Render danger correctly', async () => {
    state.aditive.veg = false;
    const store = new Vuex.Store({
      modules: {
        aditive: {
          state: state
        },
        user: {
          state: {
            userInfo: {
              dietTypes: ['veg']
            }
          }
        }
      }
    });
    wrapper = mount(AditiveInfo, {
      mocks: {
        $store: store
      },
      vuetify,
      i18n
    });
    wrapper.vm.$store.state.aditive.aditive.origin = 'Animal';
    await Vue.nextTick();
    expect(wrapper.find('[data-test="icon-sprout"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-pig"]').exists()).toBe(true);
    expect(wrapper.find('[data-test="icon-help"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="icon-flask"]').exists()).toBe(false);
    expect(wrapper.find('[data-test="danger"]').exists()).toBeTruthy();
  });

});