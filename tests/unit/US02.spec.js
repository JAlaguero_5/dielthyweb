import { mount } from '@vue/test-utils';

import LoginRegister from '@/components/login-register/LoginRegister';
import Register from '@/views/Register';

import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import { i18n } from '../../src/lang/i18n';

Vue.use(Vuetify);
Vue.use(Vuex);

describe('LoginRegister.vue', () => {

  let vuetify;
  let wrapper;
  let actions;
  let state;

  beforeEach(() => {

    actions = {
      loadUserInfo: jest.fn()
    };

    state = {
      userInfo: {
        username: ''
      }
    };

    vuetify = new Vuetify();

    const store = new Vuex.Store({
      modules: {
        user: {
          state,
          actions
        }
      }
    });

    wrapper = mount(LoginRegister, {
      mocks: {
        $store: store
      },
      vuetify,
      i18n
    });

  });

  it('Renders component without login', () => {
    expect(actions.loadUserInfo.mock.calls.length).toBe(1);
    expect(wrapper.vm.isLoggedIn).toBeFalsy();
    expect(wrapper.find('[data-test="login-text"]').exists()).toBeTruthy();
    expect(wrapper.find('[data-test="register-text"]').exists()).toBeTruthy();
    expect(wrapper.find('[data-test="icon-user"]').exists()).toBeFalsy();
    expect(wrapper.find('[data-test="username-text"]').exists()).toBeFalsy();
    expect(wrapper.find('[data-test="show-profile-text"]').exists()).toBeFalsy();
    expect(wrapper.find('[data-test="logout-text"]').exists()).toBeFalsy();
  });

  it('Renders component when login', async () => {
    expect(actions.loadUserInfo.mock.calls.length).toBe(1);
    wrapper.vm.$store.state.user.userInfo.username = 'jalaguero';
    wrapper.vm.isLoggedIn = true;
    await Vue.nextTick();
    expect(wrapper.find('[data-test="login-text"]').exists()).toBeFalsy();
    expect(wrapper.find('[data-test="register-text"]').exists()).toBeFalsy();
    expect(wrapper.find('[data-test="icon-user"]').exists()).toBeTruthy();
    expect(wrapper.find('[data-test="username-text"]').exists()).toBeTruthy();
    expect(wrapper.find('[data-test="username-text"]').text()).toBe('jalaguero');
    expect(wrapper.find('[data-test="show-profile-text"]').exists()).toBeTruthy();
    expect(wrapper.find('[data-test="logout-text"]').exists()).toBeTruthy();
  });

});

describe('Register.vue', () => {

  let vuetify;
  let wrapper;
  let actions;
  let state;

  beforeEach(() => {

    actions = {
      createUser: jest.fn(),
      saveStateRegister: jest.fn(),
      clearRegisterForm: jest.fn()
    };

    state = {
      registerForm: {
        email: '',
        username: '',
        password: '',
        password2: '',
        name: '',
        birthDate: null,
        dietTypes: []
      }
    };

    vuetify = new Vuetify();

    const store = new Vuex.Store({
      modules: {
        user: {
          state,
          actions
        }
      }
    });

    wrapper = mount(Register, {
      mocks: {
        $store: store
      },
      stubs: ['router-link'],
      vuetify,
      i18n
    });

  });

  it('Fill every required field correctly', async () => {
    expect(actions.clearRegisterForm.mock.calls.length).toBe(0);
    wrapper.vm.email = 'ejemplo@ejemplo.com';
    wrapper.vm.password = 'ejemplo';
    wrapper.vm.password2 = 'ejemplo';
    wrapper.vm.username = 'ejemplo';
    wrapper.vm.acceptedPrivacityPolicy = true;
    await Vue.nextTick();
    const button = wrapper.find('[data-test="register-button"]');
    await button.trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.valid).toBeTruthy();
    await button.trigger('click');
    await Vue.nextTick();
    expect(actions.createUser.mock.calls.length).toBe(1);
    expect(actions.createUser.mock.calls[0][1]).toEqual({
      email: 'ejemplo@ejemplo.com',
      password: 'ejemplo',
      username: 'ejemplo',
      dietTypes: ['none'],
      birthDate: null,
      name: ''
    });
  });

  it('Email not valid', async () => {
    wrapper.vm.email = 'ejemplo';
    wrapper.vm.password = 'ejemplo';
    wrapper.vm.password2 = 'ejemplo';
    wrapper.vm.username = 'ejemplo';
    wrapper.vm.acceptedPrivacityPolicy = true;
    await Vue.nextTick();
    const button = wrapper.find('[data-test="register-button"]');
    await button.trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.valid).toBeFalsy();
    await button.trigger('click');
    await Vue.nextTick();
    expect(actions.createUser.mock.calls.length).toBe(0);
  });

  it('Email empty', async () => {
    wrapper.vm.email = '';
    wrapper.vm.password = 'ejemplo';
    wrapper.vm.password2 = 'ejemplo';
    wrapper.vm.username = 'ejemplo';
    wrapper.vm.acceptedPrivacityPolicy = true;
    await Vue.nextTick();
    const button = wrapper.find('[data-test="register-button"]');
    await button.trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.valid).toBeFalsy();
    await button.trigger('click');
    await Vue.nextTick();
    expect(actions.createUser.mock.calls.length).toBe(0);
  });

  it('Password empty', async () => {
    wrapper.vm.email = 'ejemplo@ejemplo.com';
    wrapper.vm.password = '';
    wrapper.vm.password2 = 'ejemplo';
    wrapper.vm.username = 'ejemplo';
    wrapper.vm.acceptedPrivacityPolicy = true;
    await Vue.nextTick();
    const button = wrapper.find('[data-test="register-button"]');
    await button.trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.valid).toBeFalsy();
    await button.trigger('click');
    await Vue.nextTick();
    expect(actions.createUser.mock.calls.length).toBe(0);
  });

  it('Password less than 6 characters', async () => {
    wrapper.vm.email = 'ejemplo@ejemplo.com';
    wrapper.vm.password = 'ejemp';
    wrapper.vm.password2 = 'ejemplo';
    wrapper.vm.username = 'ejemplo';
    wrapper.vm.acceptedPrivacityPolicy = true;
    await Vue.nextTick();
    const button = wrapper.find('[data-test="register-button"]');
    await button.trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.valid).toBeFalsy();
    await button.trigger('click');
    await Vue.nextTick();
    expect(actions.createUser.mock.calls.length).toBe(0);
  });

  it('Passwords not the same', async () => {
    wrapper.vm.email = 'ejemplo@ejemplo.com';
    wrapper.vm.password = 'ejemplo';
    wrapper.vm.password2 = 'ejemplo2';
    wrapper.vm.username = 'ejemplo';
    wrapper.vm.acceptedPrivacityPolicy = true;
    await Vue.nextTick();
    const button = wrapper.find('[data-test="register-button"]');
    await button.trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.valid).toBeFalsy();
    await button.trigger('click');
    await Vue.nextTick();
    expect(actions.createUser.mock.calls.length).toBe(0);
  });

  it('Username empty', async () => {
    wrapper.vm.email = 'ejemplo@ejemplo.com';
    wrapper.vm.password = 'ejemplo';
    wrapper.vm.password2 = 'ejemplo';
    wrapper.vm.username = '';
    wrapper.vm.acceptedPrivacityPolicy = true;
    await Vue.nextTick();
    const button = wrapper.find('[data-test="register-button"]');
    await button.trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.valid).toBeFalsy();
    await button.trigger('click');
    await Vue.nextTick();
    expect(actions.createUser.mock.calls.length).toBe(0);
  });

  it('Not accepted privacity policy', async () => {
    wrapper.vm.email = 'ejemplo@ejemplo.com';
    wrapper.vm.password = 'ejemplo';
    wrapper.vm.password2 = 'ejemplo';
    wrapper.vm.username = 'ejemplo';
    await Vue.nextTick();
    const button = wrapper.find('[data-test="register-button"]');
    expect(button.attributes().disabled).toBeTruthy();
  });

  it('None checkbox behavior', async () => {
    const none = wrapper.find('[data-test="checkbox-none"]');
    const lac = wrapper.find('[data-test="checkbox-lac"]');
    const veg = wrapper.find('[data-test="checkbox-veg"]');
    const cel = wrapper.find('[data-test="checkbox-cel"]');

    expect(wrapper.vm.dietTypes).toEqual([]);

    await lac.trigger('change');
    await Vue.nextTick();

    await veg.trigger('change');
    await Vue.nextTick();

    expect(wrapper.vm.dietTypes).toEqual(['lac', 'veg']);

    await none.trigger('change');
    await Vue.nextTick();
    
    expect(wrapper.vm.dietTypes).toEqual(['none']);
    expect(lac.attributes().checked).toBeFalsy();
    expect(veg.attributes().checked).toBeFalsy();
    expect(cel.attributes().checked).toBeFalsy();
  });


});