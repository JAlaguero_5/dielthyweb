import { mount } from '@vue/test-utils';

import Reports from '@/views/Reports';
import ReportInfo from '../../src/components/report-info/ReportInfo';

import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import { i18n } from '../../src/lang/i18n';
import { db } from '../../src/plugins/firebase';

Vue.use(Vuetify);
Vue.use(Vuex);

describe('Reports.vue', () => {

  let vuetify;
  let wrapper;

  afterAll(async () => {
    await db.disableNetwork();
    await db.terminate();
  });

  beforeEach(() => {
    vuetify = new Vuetify();

    wrapper = mount(Reports, {
      vuetify,
      i18n
    });

  });

  it('Renders correctly', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.reports = require('./reportsMock.json');
    wrapper.vm.reportsFiltered = require('./reportsMock.json');
    await Vue.nextTick();
    expect(wrapper.findAll('[data-test="report-info"]').length).toBe(2);
    expect(wrapper.find('[data-test="pagination"').exists()).toBeFalsy();
    expect(wrapper.find('[data-test="no-reports"').exists()).toBeFalsy();
    await wrapper.findAll('.v-expansion-panel-header').at(0).trigger('click');
    await Vue.nextTick();
    await wrapper.findAll('.v-expansion-panel-header').at(1).trigger('click');
    await Vue.nextTick();
    expect(wrapper.findAll('[data-test="report-info"]').at(0).findAll('[data-test="users-report"]').length).toBe(2);
    expect(wrapper.findAll('[data-test="report-info"]').at(1).findAll('[data-test="users-report"]').length).toBe(1);
  });

  it('No reports', async () => {
    wrapper.vm.loading = false;
    await Vue.nextTick();
    expect(wrapper.find('[data-test="no-reports"]').exists()).toBeTruthy();
    expect(wrapper.findAll('[data-test="report-info"]').length).toBe(0);
  });

  it('Search user', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.reports = require('./reportsMock.json');
    wrapper.vm.reportsFiltered = require('./reportsMock.json');
    await Vue.nextTick();
    await wrapper.find('[data-test="filter-user"]').setValue('2');
    await Vue.nextTick();
    wrapper.vm.filter();
    await Vue.nextTick();
    expect(wrapper.findAll('[data-test="report-info"]').length).toBe(1);
  });

});

describe('ReportInfo.vue', () => {
  
  let vuetify;
  let wrapper;

  afterAll(async () => {
    await db.disableNetwork();
    await db.terminate();
  });

  beforeEach(() => {
    vuetify = new Vuetify();

    wrapper = mount(ReportInfo, {
      vuetify,
      i18n,
      propsData: {
        info: require('./reportInfoMock.json')
      }
    });

  });

  it('Renders correctly', async () => {
    const numberOfUsers = Number(wrapper.find('[data-test="number-users"]').text().split(':')[1].trim());
    await wrapper.find('[data-test="number-users"]').trigger('click');
    await Vue.nextTick();
    expect(wrapper.findAll('[data-test="users-report"]').length).toBe(numberOfUsers);
  });

});