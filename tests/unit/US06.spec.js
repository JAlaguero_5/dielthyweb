import { mount } from '@vue/test-utils';

import Profile from '../../src/views/Profile';

import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import { i18n } from '../../src/lang/i18n';
import { db } from '../../src/plugins/firebase';

Vue.use(Vuetify);
Vue.use(Vuex);

describe('Delete user button', () => {

  let vuetify;
  let wrapper;
  let actions;
  let state;

  beforeAll(async () => {
    await db.enableNetwork();
  });

  afterAll(async () => {
    await db.disableNetwork();
    await db.terminate();
  });

  beforeEach(() => {

    vuetify = new Vuetify();

    actions = {
      loadUserInfo: jest.fn()
    };

    state = {
      userInfo: {
        username: '',
        email: '',
        name: '',
        birthDate: null,
        dietTypes: [],
        emailVerified: false,
        uid: 'mock-uid',
        admin: true
      }
    };

    const store = new Vuex.Store({
      modules: {
        user: {
          state,
          actions
        }
      }
    });

    const route = {
      params: {
        id: '123456789'
      }
    };

    wrapper = mount(Profile, {
      mocks: {
        $store: store,
        $route: route
      },
      vuetify,
      i18n
    });

  });

  it('Renders when is admin and is a different user', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.differentUser = {
      usuario: '',
      email: '',
      nombre: '',
      fechaNacimiento: '',
      tiposDieta: [],
      uid: '123456789'
    };
    await Vue.nextTick();
    expect(wrapper.find('[data-test="delete-button"]').exists()).toBeTruthy();
  });

  it('Not renders when is not admin', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.differentUser = {
      usuario: '',
      email: '',
      nombre: '',
      fechaNacimiento: '',
      tiposDieta: [],
      uid: '123456789'
    };
    wrapper.vm.$store.state.user.userInfo.admin = false;
    await Vue.nextTick();
    expect(wrapper.find('[data-test="delete-button"]').exists()).toBeFalsy();
  });

  it('Not renders when is same user', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.$store.state.user.userInfo.uid = '123456789';
    await Vue.nextTick();
    expect(wrapper.find('[data-test="delete-button"]').exists()).toBeFalsy();
  });

});