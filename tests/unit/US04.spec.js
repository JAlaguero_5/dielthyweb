import { mount } from '@vue/test-utils';

import Profile from '../../src/views/Profile';

import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import { i18n } from '../../src/lang/i18n';

Vue.use(Vuetify);
Vue.use(Vuex);

describe('Profile.vue', () => {
  
  let vuetify;
  let wrapper;
  let actions;
  let state;

  beforeEach(() => {

    vuetify = new Vuetify();

    actions = {
      updateUserInfo: jest.fn(),
      loadUserInfo: jest.fn()
    };

    state = {
      userInfo: {
        username: '',
        email: '',
        name: '',
        birthDate: null,
        dietTypes: [],
        emailVerified: false,
        uid: 'mock-uid'
      }
    };

    const store = new Vuex.Store({
      modules: {
        user: {
          state,
          actions
        }
      }
    });

    const route = {
      params: {
        id: 'mock-uid'
      }
    };

    wrapper = mount(Profile, {
      mocks: {
        $store: store,
        $route: route
      },
      vuetify,
      i18n
    });

  });

  it('Shows correctly profile info', async () => {
    wrapper.vm.$store.state.user.userInfo.username = 'jalaguero';
    wrapper.vm.$store.state.user.userInfo.name = 'Javier';
    wrapper.vm.$store.state.user.userInfo.birthDate = '01-01-1970';
    wrapper.vm.$store.state.user.userInfo.dietTypes = ['none'];
    wrapper.vm.$store.state.user.userInfo.email = 'jalaguero5@gmail.com';
    wrapper.vm.$store.state.user.userInfo.emailVerified = false;
    await Vue.nextTick();
    expect(wrapper.find('[data-test="verified-subheader"]').exists()).toBeTruthy();
    expect(wrapper.find('[data-test="show-profile-username"]').exists()).toBeTruthy();
    expect(wrapper.find('[data-test="show-profile-username"]').text()).toBe('jalaguero');
    expect(wrapper.find('[data-test="dietTypes-show-profile0"]').text()).toBe('Ninguno');
    expect(wrapper.find('[data-test="name-show-profile"]').text()).toBe('Javier');
    expect(wrapper.find('[data-test="birthDate-show-profile"]').text()).toBe('01-01-1970');
    expect(wrapper.find('[data-test="email-show-profile"]').text()).toBe('jalaguero5@gmail.com');
  });

  it('Shows correctly profile info when editing', async () => {
    wrapper.vm.$store.state.user.userInfo.username = 'jalaguero';
    wrapper.vm.$store.state.user.userInfo.name = 'Javier';
    wrapper.vm.$store.state.user.userInfo.birthDate = '20-10-2018';
    wrapper.vm.$store.state.user.userInfo.dietTypes = ['none'];
    wrapper.vm.$store.state.user.userInfo.email = 'jalaguero5@gmail.com';
    wrapper.vm.$store.state.user.userInfo.emailVerified = true;
    await wrapper.find('[data-test="edit-button"]').trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.editInfo.username).toBe('jalaguero');
    await Vue.nextTick();
    expect(wrapper.vm.editInfo.name).toBe('Javier');
    await Vue.nextTick();
    expect(wrapper.vm.editInfo.formattedDate).toBe('20-10-2018');
    await Vue.nextTick();
    expect(wrapper.vm.editInfo.dietTypes).toEqual(['none']);
    await Vue.nextTick();
    expect(wrapper.vm.isEdit).toBeTruthy();
    await Vue.nextTick();
    expect(wrapper.find('[data-test="verified-subheader"]').exists()).toBeFalsy();
    expect(wrapper.find('[data-test="show-profile-username"]').exists()).toBeFalsy();    
  });

  it('Correct form without password', async () => {
    wrapper.vm.$store.state.user.userInfo.username = 'jalaguero';
    wrapper.vm.$store.state.user.userInfo.name = 'Javier';
    wrapper.vm.$store.state.user.userInfo.birthDate = '01-01-1970';
    wrapper.vm.$store.state.user.userInfo.dietTypes = ['none'];
    wrapper.vm.$store.state.user.userInfo.email = 'jalaguero5@gmail.com';
    wrapper.vm.$store.state.user.userInfo.emailVerified = true;
    await wrapper.find('[data-test="edit-button"]').trigger('click');
    await Vue.nextTick();
    await wrapper.find('[data-test="save-button"]').trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.valid).toBeTruthy();
    expect(actions.updateUserInfo.mock.calls.length).toBe(1);
    await Vue.nextTick();
    expect(actions.loadUserInfo.mock.calls.length).toBe(1);
  });

});