import { mount } from '@vue/test-utils';

import Reports from '@/views/Reports';

import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import { i18n } from '../../src/lang/i18n';
import { db } from '../../src/plugins/firebase';

Vue.use(Vuetify);
Vue.use(Vuex);

describe('Report a comment and dismiss reports', () => {

  let vuetify;
  let wrapper;

  beforeAll(async () => {
    await db.enableNetwork();
  });

  afterAll(async () => {
    await db.disableNetwork();
    await db.terminate();
  });

  beforeEach(() => {
    vuetify = new Vuetify();

    wrapper = mount(Reports, {
      vuetify,
      i18n
    });

  });

  it('Renders correctly dismiss button', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.reports = require('./reportsMock.json');
    wrapper.vm.reportsFiltered = require('./reportsMock.json');
    await Vue.nextTick();
    expect(wrapper.findAll('[data-test="report-info"]').length).toBe(2);
    expect(wrapper.findAll('[data-test="report-info"]').at(0).find('[data-test="dismiss-comment"]').exists()).toBeTruthy();
    expect(wrapper.findAll('[data-test="report-info"]').at(1).find('[data-test="dismiss-comment"]').exists()).toBeTruthy();
  });

});