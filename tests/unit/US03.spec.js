import { mount } from '@vue/test-utils';

import Login from '../../src/views/Login';

import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import { i18n } from '../../src/lang/i18n';

Vue.use(Vuetify);
Vue.use(Vuex);

describe('Login.vue', () => {

  let vuetify;
  let wrapper;
  let actions;

  beforeEach(() => {

    actions = {
      login: jest.fn()
    };

    vuetify = new Vuetify();

    const store = new Vuex.Store({
      modules: {
        user: {
          actions
        }
      }
    });

    wrapper = mount(Login, {
      mocks: {
        $store: store
      },
      vuetify,
      i18n
    });

  });

  it('Email empty', async () => {
    await wrapper.find('[data-test="password-login"]').setValue('password');
    await Vue.nextTick();
    await wrapper.find('[data-test="login-button"]').trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.valid).toBeFalsy();
  });

  it('Email not valid', async () => {
    await wrapper.find('[data-test="login-email"]').setValue('a');
    await Vue.nextTick();
    await wrapper.find('[data-test="password-login"]').setValue('password');
    await Vue.nextTick();
    await wrapper.find('[data-test="login-button"]').trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.valid).toBeFalsy();
  });

  it('Password empty', async () => {
    await wrapper.find('[data-test="login-email"]').setValue('jalaguero5@gmail.com');
    await Vue.nextTick();
    await wrapper.find('[data-test="login-button"]').trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.valid).toBeFalsy();
  });

  it('Login correct', async () => {
    await wrapper.find('[data-test="login-email"]').setValue('jalaguero5@gmail.com');
    await Vue.nextTick();
    await wrapper.find('[data-test="password-login"]').setValue('password');
    await Vue.nextTick();
    await wrapper.find('[data-test="login-button"]').trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.valid).toBeTruthy();
    await wrapper.find('[data-test="login-button"]').trigger('click');
    await Vue.nextTick();
    expect(actions.login.mock.calls.length).toBe(2);
  });

  it('Login fails', async () => {
    expect(wrapper.find('[data-test="show-error"]').isVisible()).toBeFalsy();
    await wrapper.find('[data-test="login-email"]').setValue('jalaguero5@gmail.com');
    await Vue.nextTick();
    await wrapper.find('[data-test="password-login"]').setValue('password');
    await Vue.nextTick();
    await wrapper.find('[data-test="login-button"]').trigger('click');
    await Vue.nextTick();
    expect(wrapper.vm.valid).toBeTruthy();
    await wrapper.find('[data-test="login-button"]').trigger('click');
    await Vue.nextTick();
    expect(actions.login.mock.calls.length).toBe(2);
    wrapper.vm.error = true;
    await Vue.nextTick();
    expect(wrapper.find('[data-test="show-error"]').isVisible()).toBeTruthy();
  });

});