import { mount } from '@vue/test-utils';

import Topic from '../../src/views/Topic';
import CommentTopic from '../../src/components/comment-topic/CommentTopic';

import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import { i18n } from '../../src/lang/i18n';
import { db } from '../../src/plugins/firebase';

Vue.use(Vuetify);
Vue.use(Vuex);

describe('Topic.vue', () => {

  let vuetify;
  let wrapper;
  let state;

  beforeAll(async () => {
    await db.enableNetwork();
  });

  afterAll(async () => {
    await db.disableNetwork();
    await db.terminate();
  });

  beforeEach(() => {
    vuetify = new Vuetify();

    state = {
      userInfo: {
        uid: '',
        username: ''
      }
    };

    const store = new Vuex.Store({
      modules: {
        user: {
          state
        }
      }
    });

    const route = {
      params: {
        id: 'mock-uid'
      }
    };

    wrapper = mount(Topic, {
      vuetify,
      i18n,
      mocks: {
        $route: route,
        $store: store
      }
    });
  });
  

  it('No comments', async () => {
    wrapper.vm.loading = false;
    await Vue.nextTick();
    expect(wrapper.find('.no-comments').exists()).toBeTruthy();
    expect(wrapper.find('.topic-content__comments').exists()).toBeFalsy();
  });

  it('Comments and its answers', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.comments = require('./commentsMock.json');
    await Vue.nextTick();
    expect(wrapper.find('.no-comments').exists()).toBeFalsy();
    expect(wrapper.find('.topic-content__comments').exists()).toBeTruthy();
    expect(wrapper.findAll('[data-test="comment"]').length).toBe(2);
    expect(wrapper.findAll('[data-test="comment"]').at(1).findAll('[data-test="answer"]').length).toBe(3);
    expect(wrapper.find('[data-test="pagination"]').exists()).toBeFalsy();
  });

});

describe('CommentTopic.vue', () => {
  
  let vuetify;
  let wrapper;
  let state;

  beforeEach(() => {

    vuetify = new Vuetify();

    state = {
      userInfo: {
        uid: ''
      }
    };

    const store = new Vuex.Store({
      modules: {
        user: {
          state
        }
      }
    });

    wrapper = mount(CommentTopic, {
      vuetify,
      i18n,
      propsData: {
        info: require('./commentsAnswersMock.json'),
        isAnswer: false
      },
      mocks: {
        $store: store
      }
    });
  });

  it('Renders correctly', () => {
    expect(wrapper.findAll('[data-test="reply-button"]').length).toBe(1);
    expect(wrapper.findAll('[data-test="answer"]').length).toBe(3);
    expect(wrapper.findAll('[data-test="answer"]').at(0).find('[data-test="reply-button"]').exists()).toBeFalsy();
    expect(wrapper.findAll('[data-test="answer"]').at(1).find('[data-test="reply-button"]').exists()).toBeFalsy();
    expect(wrapper.findAll('[data-test="answer"]').at(2).find('[data-test="reply-button"]').exists()).toBeFalsy();
  });

});
