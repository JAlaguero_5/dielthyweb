import { mount } from '@vue/test-utils';

import TopicTag from '../../src/components/topic-tag/TopicTag';
import Forum from '../../src/views/Forum';

import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import { i18n } from '../../src/lang/i18n';
import { db } from '../../src/plugins/firebase';

Vue.use(Vuetify);
Vue.use(Vuex);

describe('TopicTag.vue', () => {

  let vuetify;
  let wrapper;

  afterAll(async () => {
    await db.disableNetwork();
    await db.terminate();
  });

  beforeEach(() => {

    vuetify = new Vuetify();

    wrapper = mount(TopicTag, {
      vuetify,
      i18n
    });

  });

  it('Renders veg tag correctly', async () => {
    await wrapper.setProps({ tag: 'veg' });
    await Vue.nextTick();
    expect(wrapper.find('[data-test="topic-tag"]').text()).toBe('Vegano');
    expect(wrapper.find('[data-test="topic-tag"]').classes('veg')).toBeTruthy();
    expect(wrapper.find('[data-test="topic-tag"]').classes('cel')).toBeFalsy();
    expect(wrapper.find('[data-test="topic-tag"]').classes('lac')).toBeFalsy();
    expect(wrapper.find('[data-test="topic-tag"]').classes('none')).toBeFalsy();
  });

  it('Renders cel tag correctly', async () => {
    await wrapper.setProps({ tag: 'cel' });
    await Vue.nextTick();
    expect(wrapper.find('[data-test="topic-tag"]').text()).toBe('Celíaco');
    expect(wrapper.find('[data-test="topic-tag"]').classes('veg')).toBeFalsy();
    expect(wrapper.find('[data-test="topic-tag"]').classes('cel')).toBeTruthy();
    expect(wrapper.find('[data-test="topic-tag"]').classes('lac')).toBeFalsy();
    expect(wrapper.find('[data-test="topic-tag"]').classes('none')).toBeFalsy();
  });

  it('Renders lac tag correctly', async () => {
    await wrapper.setProps({ tag: 'lac' });
    await Vue.nextTick();
    expect(wrapper.find('[data-test="topic-tag"]').text()).toBe('Lactosa');
    expect(wrapper.find('[data-test="topic-tag"]').classes('veg')).toBeFalsy();
    expect(wrapper.find('[data-test="topic-tag"]').classes('cel')).toBeFalsy();
    expect(wrapper.find('[data-test="topic-tag"]').classes('lac')).toBeTruthy();
    expect(wrapper.find('[data-test="topic-tag"]').classes('none')).toBeFalsy();
  });

  it('Renders none tag correctly', async () => {
    await wrapper.setProps({ tag: 'none' });
    await Vue.nextTick();
    expect(wrapper.find('[data-test="topic-tag"]').text()).toBe('General');
    expect(wrapper.find('[data-test="topic-tag"]').classes('veg')).toBeFalsy();
    expect(wrapper.find('[data-test="topic-tag"]').classes('cel')).toBeFalsy();
    expect(wrapper.find('[data-test="topic-tag"]').classes('lac')).toBeFalsy();
    expect(wrapper.find('[data-test="topic-tag"]').classes('none')).toBeTruthy();
  });

});

describe('Forum.vue', () => {

  let vuetify;
  let wrapper;

  afterAll(async () => {
    await db.disableNetwork();
    await db.terminate();
  });

  beforeEach(() => {

    vuetify = new Vuetify();

    wrapper = mount(Forum, {
      vuetify,
      i18n
    });

  });

  it('No topics', async () => {
    wrapper.vm.loading = false;
    await Vue.nextTick();
    expect(wrapper.find('[data-test="no-topic"]').exists()).toBeTruthy();
    expect(wrapper.find('[data-test="topics-info"]').exists()).toBeFalsy();
    expect(wrapper.find('[data-test="pagination"]').exists()).toBeFalsy();
  });

  it('Topics neither ordered nor filtered', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.topics = require('./topicsMock.json');
    wrapper.vm.topicsFiltered = require('./topicsMock.json');
    await Vue.nextTick();
    expect(wrapper.find('[data-test="no-topic"]').exists()).toBeFalsy();
    expect(wrapper.find('[data-test="topics-info"]').exists()).toBeTruthy();
    expect(wrapper.find('[data-test="pagination"]').exists()).toBeFalsy();
    expect(wrapper.findAll('[data-test="topic"]').length).toBe(4);
  });

  it('Topics ordered by update date descending', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.topicsFiltered = require('./topicsMock.json');
    wrapper.vm.topics = require('./topicsMock.json');
    wrapper.vm.order = 'actualizado';
    await Vue.nextTick();
    wrapper.vm.filterAndOrder();
    await Vue.nextTick(); 
    const mock = require('./topicsOrderedByUpdateDescending.json');
    expect(wrapper.vm.topicsList).toEqual(mock);
  });

  it('Topics ordered by update date ascending', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.topicsFiltered = require('./topicsMock.json');
    wrapper.vm.topics = require('./topicsMock.json');
    wrapper.vm.order = 'actualizado';
    wrapper.vm.orderCriteria = 1;
    await Vue.nextTick();
    wrapper.vm.filterAndOrder();
    await Vue.nextTick(); 
    const mock = require('./topicsOrderedByUpdateAscending.json');
    expect(wrapper.vm.topicsList).toEqual(mock);
  });

  it('Topics ordered by creation date descending', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.topicsFiltered = require('./topicsMock.json');
    wrapper.vm.topics = require('./topicsMock.json');
    wrapper.vm.order = 'creado';
    await Vue.nextTick();
    wrapper.vm.filterAndOrder();
    await Vue.nextTick(); 
    const mock = require('./topicsOrderedByCreationDescending.json');
    expect(wrapper.vm.topicsList).toEqual(mock);
  });

  it('Topics ordered by creation date ascending', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.topicsFiltered = require('./topicsMock.json');
    wrapper.vm.topics = require('./topicsMock.json');
    wrapper.vm.order = 'creado';
    wrapper.vm.orderCriteria = 1;
    await Vue.nextTick();
    wrapper.vm.filterAndOrder();
    await Vue.nextTick(); 
    const mock = require('./topicsOrderedByCreationAscending.json');
    expect(wrapper.vm.topicsList).toEqual(mock);
  });

  it('Topics filtered by veg tag', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.topicsFiltered = require('./topicsMock.json');
    wrapper.vm.topics = require('./topicsMock.json');
    wrapper.vm.filter = ['veg'];
    await Vue.nextTick();
    wrapper.vm.filterAndOrder();
    await Vue.nextTick();
    const mock = [{
      nombre: 'Nombre tema 1',
      creado: '10-03-2020',
      actualizado: '16-03-2020 10:00',
      mens: 20,
      usuarioCreacion: 'jalaguero',
      uidCreacion: '0ReHtMzyLMhDcH4aDmp77mW50dX2',
      tag: ['veg'],
      idioma: 'es'
    }];
    expect(wrapper.vm.topicsList).toEqual(mock);
  });

  it('Topics filtered by cel tag', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.topicsFiltered = require('./topicsMock.json');
    wrapper.vm.topics = require('./topicsMock.json');
    wrapper.vm.filter = ['cel'];
    await Vue.nextTick();
    wrapper.vm.filterAndOrder();
    await Vue.nextTick();
    const mock = [{
      nombre: 'Nombre tema 2',
      creado: '11-03-2020',
      actualizado: '17-03-2020 10:00',
      mens: 15,
      usuarioCreacion: 'jalaguero',
      uidCreacion: '0ReHtMzyLMhDcH4aDmp77mW50dX2',
      tag: ['cel'],
      idioma: 'en'
    }];
    expect(wrapper.vm.topicsList).toEqual(mock);
  });

  it('Topics filtered by lac tag', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.topicsFiltered = require('./topicsMock.json');
    wrapper.vm.topics = require('./topicsMock.json');
    wrapper.vm.filter = ['lac'];
    await Vue.nextTick();
    wrapper.vm.filterAndOrder();
    await Vue.nextTick();
    const mock = [{
      nombre: 'Nombre tema 4',
      creado: '17-03-2020',
      actualizado: '23-03-2020 10:00',
      mens: 26,
      usuarioCreacion: 'jalaguero',
      uidCreacion: '0ReHtMzyLMhDcH4aDmp77mW50dX2',
      tag: ['lac'],
      idioma: 'en'
    }];
    expect(wrapper.vm.topicsList).toEqual(mock);
  });

  it('Topics filtered by none tag', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.topicsFiltered = require('./topicsMock.json');
    wrapper.vm.topics = require('./topicsMock.json');
    wrapper.vm.filter = ['none'];
    await Vue.nextTick();
    wrapper.vm.filterAndOrder();
    await Vue.nextTick();
    const mock = [{
      nombre: 'Nombre tema 3',
      creado: '12-03-2020',
      actualizado: '18-03-2020 10:00',
      mens: 25,
      usuarioCreacion: 'jalaguero',
      uidCreacion: '0ReHtMzyLMhDcH4aDmp77mW50dX2',
      tag: ['none'],
      idioma: 'es'
    }];
    expect(wrapper.vm.topicsList).toEqual(mock);
  });

  it('Topics filtered by name', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.topicsFiltered = require('./topicsMock.json');
    wrapper.vm.topics = require('./topicsMock.json');
    wrapper.vm.filterTopicName = '3';
    await Vue.nextTick();
    wrapper.vm.filterAndOrder();
    await Vue.nextTick();
    const mock = [{
      nombre: 'Nombre tema 3',
      creado: '12-03-2020',
      actualizado: '18-03-2020 10:00',
      mens: 25,
      usuarioCreacion: 'jalaguero',
      uidCreacion: '0ReHtMzyLMhDcH4aDmp77mW50dX2',
      tag: ['none'],
      idioma: 'es'
    }];
    expect(wrapper.vm.topicsList).toEqual(mock);
  });

  it('Topics filtered by spanish language', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.topicsFiltered = require('./topicsMock.json');
    wrapper.vm.topics = require('./topicsMock.json');
    wrapper.vm.filterByLanguage = 'es';
    await Vue.nextTick();
    wrapper.vm.filterAndOrder();
    await Vue.nextTick();
    const mock = [
      {
        nombre: 'Nombre tema 3',
        creado: '12-03-2020',
        actualizado: '18-03-2020 10:00',
        mens: 25,
        usuarioCreacion: 'jalaguero',
        uidCreacion: '0ReHtMzyLMhDcH4aDmp77mW50dX2',
        tag: ['none'],
        idioma: 'es'
      },
      {
        nombre:'Nombre tema 1',
        creado: '10-03-2020',
        actualizado: '16-03-2020 10:00',
        mens: 20,
        usuarioCreacion: 'jalaguero',
        uidCreacion: '0ReHtMzyLMhDcH4aDmp77mW50dX2',
        tag: ['veg'],
        idioma: 'es'
      }
    ];
    expect(wrapper.vm.topicsList).toEqual(mock);
  });

  it('Topics filtered by english language', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.topicsFiltered = require('./topicsMock.json');
    wrapper.vm.topics = require('./topicsMock.json');
    wrapper.vm.filterByLanguage = 'en';
    await Vue.nextTick();
    wrapper.vm.filterAndOrder();
    await Vue.nextTick();
    const mock = [
      {
        nombre:'Nombre tema 4',
        creado: '17-03-2020',
        actualizado: '23-03-2020 10:00',
        mens: 26,
        usuarioCreacion: 'jalaguero',
        uidCreacion: '0ReHtMzyLMhDcH4aDmp77mW50dX2',
        tag: ['lac'],
        idioma: 'en'
      },
      {
        nombre: 'Nombre tema 2',
        creado: '11-03-2020',
        actualizado: '17-03-2020 10:00',
        mens: 15,
        usuarioCreacion: 'jalaguero',
        uidCreacion: '0ReHtMzyLMhDcH4aDmp77mW50dX2',
        tag: ['cel'],
        idioma: 'en'
      }
    ];
    expect(wrapper.vm.topicsList).toEqual(mock);
  });

});