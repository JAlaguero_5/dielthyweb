import { mount } from '@vue/test-utils';

import Topic from '../../src/views/Topic';

import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import { i18n } from '../../src/lang/i18n';
import { db } from '../../src/plugins/firebase';

Vue.use(Vuetify);
Vue.use(Vuex);

describe('Report comment', () => {

  let vuetify;
  let wrapper;
  let state;

  beforeAll(async () => {
    await db.enableNetwork();
  });

  afterAll(async () => {
    await db.disableNetwork();
    await db.terminate();
  });

  beforeEach(() => {

    vuetify = new Vuetify();

    const route = {
      params: {
        id: 'mock-uid'
      }
    };

    state = {
      userInfo: {
        uid: '4vHpc377Q2MA760JkPvVtzN2jpa2',
        username: 'jalaguero2'
      }
    };

    const store = new Vuex.Store({
      modules: {
        user: {
          state
        }
      }
    });

    wrapper = mount(Topic, {
      vuetify,
      i18n,
      mocks: {
        $route: route,
        $store: store
      }
    });

  });

  it('Only comments with different uid with the report button', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.comments = require('./commentsMock.json');
    await Vue.nextTick();
    expect(wrapper.findAll('[data-test="report-button"]').length).toBe(5);
  });

});