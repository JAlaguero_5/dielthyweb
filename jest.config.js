module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  setupFilesAfterEnv: ['./jest.setup.js'],
  collectCoverage: true,
  collectCoverageFrom: ['**/*.{js,vue}', '!**/node_modules/**'],
  coveragePathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/dist/', '<rootDir>/tests/',
  '<rootDir>/src/plugins/', '<rootDir>/src/tests/', '<rootDir>/src/utils', '<rootDir>/src/router/',
  '<rootDir>/coverage/', '<rootDir>/src/lang', '<rootDir>/src/store', '<rootDir>/src/api',
  '<rootDir>/src/App.vue', '<rootDir>/src/main.js', '<rootDir>/babel.config.js', '<rootDir>/jest.config.js',
  '<rootDir>/instrumented', '<rootDir>/jest.setup.js', '<rootDir>/nyc.config.js']
};
