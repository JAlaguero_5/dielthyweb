import Vue from 'vue';
import VueRouter from 'vue-router';
import Main from '@/views/Main';
import AditiveInfo from '@/views/AditiveInfo';
import Register from '@/views/Register';
import PrivacityPolicy from '@/views/PrivacityPolicy';
import Login from '@/views/Login';
import Profile from '@/views/Profile';
import Forum from '@/views/Forum';
import Topic from '@/views/Topic';
import Reports from '@/views/Reports';
import firebase from 'firebase/app';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Main',
    component: Main
  },
  {
    path: '/aditive',
    name: 'Aditive',
    component: AditiveInfo
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/privacity',
    name: 'PrivacityPolicy',
    component: PrivacityPolicy
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/profile/:id',
    name: 'Profile',
    component: Profile,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/forum',
    name: 'Forum',
    component: Forum,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/topic/:id',
    name: 'Topic',
    component: Topic,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/reports',
    name: 'Reports',
    component: Reports,
    meta: {
      requiresAuth: true,
      requiresAdmin: true
    }
  },
  {
    path: '*',
    redirect: '/'
  }
];

const router = new VueRouter({
  mode: 'history',
  routes
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(x => x.meta.requiresAuth);
  const currentUser = firebase.auth().currentUser;
  const requiresAdmin = to.matched.some(x => x.meta.requiresAdmin);

  if (requiresAuth && !currentUser) {
    if (from.path !== '/login') {
      next('/login');
    }
  } else if (requiresAuth && currentUser) {
    if (requiresAdmin && sessionStorage.getItem('admin') === 'false') {
      next('/');
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
