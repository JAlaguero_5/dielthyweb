export const en = {
  requiredField: '(*) This field is obligatory',
  headerBar: {
    main: 'Home',
    forum: 'Forum',
    reports: 'Reports',
    help: 'Help'
  },
  loginRegister: {
    login: 'Sign in',
    register: 'Sign up',
    showProfile: 'Profile',
    logout: 'Sign out',
    dialogLogout: {
      cancel: 'Cancel',
      accept: 'Accept',
      question: 'Do you want to sign out?'
    }
  },
  aditiveInfo: {
    aditiveCode: 'E-',
    description: 'Description: ',
    origin: 'Origin: ',
    type: 'Type: ',
    cel: 'Suitable for coeliac: ',
    veg: 'Suitable for vegan/vegetarian: ',
    lac: 'Suitable for lactose intolerant: ',
    danger: '¡Careful! This aditive is not suitable for you'
  },
  errors: {
    emptyCode: 'You should enter a code',
    noAditive: 'There is no aditive with that code'
  },
  main: {
    aditiveCode: 'Aditive code',
    welcome: '¡Welcome to Dielthy!',
    info: 'Search for food aditive you want',
    search: 'Search'
  },
  registerForm: {
    email: {
      placeholderEmail: 'example@example.com',
      label: 'Email'
    },
    password: {
      label: 'Password'
    },
    password2: {
      label: 'Confirm password'
    },
    username: {
      label: 'Username'
    },
    name: {
      label: 'Name and surname'
    },
    birthDate: {
      label: 'Birth date',
      dialog: {
        cancel: 'Cancel',
        ok: 'OK'
      }
    },
    dietTypes: {
      label: 'Diet type',
      checkbox: {
        cel: 'Coeliac',
        veg: 'Vegan or vegetarian',
        lac: 'Lactose intolerant',
        none: 'None'
      }
    },
    privacity: {
      label: 'I have read and I accept ',
      link: 'privacity policy'
    },
    completedMessages: {
      success: 'Thanks for register! Revise your email to verify your account',
      error: 'There was an unexpected error'
    },
    errors: {
      email: {
        empty: 'You have to enter an email',
        notValid: 'Email not valid'
      },
      password: {
        empty: 'You have to enter a password',
        notEquals: 'Password do not match',
        lessThanSix: 'Password have to be at least 6 characters'
      },
      username: {
        empty: 'You have to enter a username'
      }
    }
  },
  login: {
    email: {
      label: 'Email'
    },
    password: {
      label: 'Password'
    },
    loginButton: 'Sign in',
    errors: {
      email: {
        empty: 'You have to enter an email',
        notValid: 'Email not valid'
      },
      password: {
        empty: 'You have to enter a password'
      },
      notUserFound: 'Incorrect email or password',
      notEmailFound: 'Email not found',
      forgottenPassword: 'Did you forget your password?'
    },
    resetPassword: {
      labelEmail: 'Email',
      send: 'Send',
      cancel: 'Cancel',
      emptyEmail: 'Enter an email',
      emailSent: 'An email has been sent to that email address'
    }
  },
  privacityPolicy: {
    goBack: 'Back',
    title: 'PRIVACITY POLICY',
    infoSubtitle: 'This Privacity Policy establish the terms in which Dielthy use and protect ' + 
      'information that is provided by its users when they use this website. ' + 
      'This company is committed to its users data security. When we ask you for ' +
      'filling personal information fields with which you can be identified, we do it ' +
      'claiming that it only be used according to this document terms. Therefore, this ' +
      'Privacity Policy can change over time or be updated, so we strongly recommend you ' +
      'to revise continually this page to ensure that you agree with the changes.',
    info: 'Information gotten',
    informationType: 'Our website can get personal information, such as: name and surname, ' +
      'birth date, diet type, contact information sucha as your email address' +
      '... Information relative to name and surname, email address, birth date ' +
      'and diet type will be public to every user registered in the web.',
    informationUse: 'Information usage',
    informationUseObjective: 'Our website use the information to provide the best '+
      'possible service, particularly to have a user registry, and improve our products '+
      'and services.',
    compromise: 'Dielthy is highly committed to fulfil with the commitment of keep your information safe. '+
      'We use the most advanced systems and we update them constantly to '+
      'ensure that there is no unauthorised access.',
    links: 'Links to third parties',
    linksInfo: 'This website could contain links to other sites that that could be interesting for you. '+
      'Once you click on that links and leave our page, we have no control over the site '+
      'that you are redirected to and so we are neither responsible for the terms or privacity nor '+
      'your information protection in those websites. Those have its own privacity policies, '+
      'so it is recommend that you check it to confirm that you agree with them.',
    informationControlTitle: 'Personal information control',
    restrictInfo: 'You can limit information that you provide us or its usage. ',
    notSellInfo: 'This company will not sell, transfer or distribute your personal information '+
      'without your consent, except it is required by a judge with a court order.',
    rightToChange: 'Dielthy has rights to change this Privacity Policy terms at any moment.'
  },
  showProfile: {
    labels: {
      email: 'Email',
      password: 'New password',
      name: 'Name and surname',
      empty: 'Not specified',
      dietTypes: 'Diet type',
      birthDate: 'Birth date',
      password2: 'Confirm password',
      username: 'Username',
      edit: 'Edit',
      delete: 'Delete',
      dialogDelete: {
        question: 'Do you want to delete this user?',
        confirm: 'Confirm',
        cancel: 'Cancel',
        reason: 'Reason'
      }
    },
    buttons: {
      ok: 'OK',
      cancel: 'Cancel',
      save: 'Save',
      notVerified: {
        info: 'Your email has not been validated yet. ',
        link: 'Click to resend the verification email'
      }
    },
    errors: {
      password: 'Password have to be at least 6 characters',
      notEquals: 'Passwords do not match',
      updated: 'There was an error updating your information',
      deleted: 'There was an error deleting the user',
      notFound: 'User not found',
      reasonEmpty: 'You have to enter a reason'
    },
    emailSent: 'Email has been sent correctly',
    updated: 'The information has been updated',
    deleted: 'User deleted correctly',
    deletedUser: 'This user has been deleted. Reason: {reason}'
  },
  forum: {
    topic: {
      created: 'Created in {date} by ',
      updated: 'Updated {date}',
      tag: {
        veg: 'Vegan',
        cel: 'Coeliac',
        lac: 'Lactose',
        none: 'General'
      }
    },
    noTopics: 'Topics were not found',
    newTopic: 'New topic',
    filters: {
      title: 'Filters',
      order: 'Order by',
      name: 'Filter by name',
      filterBy: 'Filter by diet',
      language: 'Language',
      orderItems: {
        updated: 'Last updated',
        created: 'Creation date'
      },
      myDiet: 'My diet'
    },
    addTopic: {
      save: 'Add',
      close: 'Close',
      tag: 'Choose a tag',
      language: 'Choose a language',
      name: 'Topic name',
      errors: {
        name: 'You have to enter a name',
        tag: 'You have to enter a tag',
        language: 'You have to enter a language',
        notAdded: 'Topic could not be added'
      },
      success: 'Topic added correctly'
    },
    comments: {
      created: 'Created in {date}',
      commentButton: 'Add comment',
      replyButton: 'Answer',
      addCommentPlaceholder: 'Comment',
      addReplyPlaceholder: 'Answer',
      error: 'You have to enter a comment',
      save: 'Publish',
      close: 'Close',
      noComments: 'No comments',
      reportButton: 'Report',
      reportOk: 'Comment reported successfully',
      reportError: 'Comment could not be reported',
      dialogReport: {
        cancel: 'Cancel',
        confirm: 'Confirm',
        question: '¿Do you want to report this comment?'
      },
      tooltipDelete: 'Delete',
      dialogDelete: {
        question: 'Do you want to delete this comment?',
        cancel: 'Cancel',
        confirm: 'Confirm'
      },
      deleteOk: 'Comment deleted',
      deleteError: 'Comment could not be deleted'
    }
  },
  reports: {
    filters: {
      username: 'Filter by username'
    },
    noReports: 'No reports',
    numberOfReports: 'Reports: {num}',
    numberOfUsers: 'User that reported: {num}',
    commentUser: 'Commented by ',
    commentDate: 'Comment date: {date}',
    dialogDelete: {
      confirm: 'Confirm',
      cancel: 'Cancel',
      question: 'Do you want to delete this comment?'
    },
    dialogDismiss: {
      confirm: 'Confirm',
      cancel: 'Cancel',
      question: 'Do you want to dismiss reports of this comments?'
    },
    errorDelete: 'Comment could not be deleted',
    errorDismiss: 'Comment reports could not be dismissed',
    deleted: 'Comment deleted successfully',
    dismissed: 'Reports dismissed successfully',
    tooltipDelete: 'Delete',
    tooltipDismiss: 'Dismiss'
  },
  license: 'Product under Creative Commons License',
  language: {
    spanish: 'Español',
    english: 'English'
  }
};