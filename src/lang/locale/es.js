export const es = {
  requiredField: '(*) Este campo es obligatorio',
  headerBar: {
    main: 'Inicio',
    forum: 'Foro',
    reports: 'Denuncias',
    help: 'Ayuda'
  },
  loginRegister: {
    login: 'Iniciar sesión',
    register: 'Registrarse',
    showProfile: 'Ver perfil',
    logout: 'Cerrar sesión',
    dialogLogout: {
      cancel: 'Cancelar',
      accept: 'Aceptar',
      question: '¿Quieres cerrar sesión?'
    }
  },
  aditiveInfo: {
    aditiveCode: 'E-',
    description: 'Descripción: ',
    origin: 'Origen: ',
    type: 'Tipo: ',
    cel: 'Apto para celíacos: ',
    veg: 'Apto para veganos/vegetarianos: ',
    lac: 'Apto para intolerantes a la lactosa: ',
    danger: '¡Cuidado! Este aditivo no es adecuado para ti'
  },
  errors: {
    emptyCode: 'Se debe introducir un código',
    noAditive: 'No existe ningún aditivo con ese código'
  },
  main: {
    aditiveCode: 'Código del aditivo',
    welcome: '¡Bienvenido a Dielthy!',
    info: 'Consulta aquí el aditivo que desees',
    search: 'Buscar'
  },
  registerForm: {
    email: {
      placeholderEmail: 'ejemplo@ejemplo.com',
      label: 'Email'
    },
    password: {
      label: 'Contraseña'
    },
    password2: {
      label: 'Confirmar contraseña'
    },
    username: {
      label: 'Nombre de usuario'
    },
    name: {
      label: 'Nombre y apellidos'
    },
    birthDate: {
      label: 'Fecha de nacimiento',
      dialog: {
        cancel: 'Cancelar',
        ok: 'OK'
      }
    },
    dietTypes: {
      label: 'Tipos de dieta',
      checkbox: {
        cel: 'Celíaco/a',
        veg: 'Vegano/a o vegetariano/a',
        lac: 'Intolerante a la lactosa',
        none: 'Ninguno'
      }
    },
    privacity: {
      label: 'He leído y acepto la ',
      link: 'política de privacidad'
    },
    completedMessages: {
      success: '¡Gracias por registrarte! Revisa tu email para verificar tu cuenta',
      error: 'Ha ocurrido un error inesperado'
    },
    errors: {
      email: {
        empty: 'El email no puede estar vacío',
        notValid: 'Email no válido'
      },
      password: {
        empty: 'La contraseña no puede estar vacía',
        notEquals: 'Las contraseñas no coinciden',
        lessThanSix: 'La contraseña debe tener al menos 6 caracteres'
      },
      username: {
        empty: 'El nombre de usuario no puede estar vacío'
      }
    }
  },
  login: {
    email: {
      label: 'Email'
    },
    password: {
      label: 'Contraseña'
    },
    loginButton: 'Iniciar sesión',
    errors: {
      email: {
        empty: 'El email no puede estar vacío',
        notValid: 'Email no válido'
      },
      password: {
        empty: 'La contraseña no puede estar vacía'
      },
      notUserFound: 'Email o contraseña incorrectos',
      notEmailFound: 'No se ha encontrado el email',
      forgottenPassword: '¿Has olvidado la contraseña?'
    },
    resetPassword: {
      labelEmail: 'Email',
      send: 'Enviar',
      cancel: 'Cancelar',
      emptyEmail: 'Introduce un email',
      emailSent: 'Se ha enviado un email a la dirección indicada'
    }
  },
  privacityPolicy: {
    goBack: 'Atrás',
    title: 'POLÍTICA DE PRIVACIDAD',
    infoSubtitle: 'La presente Política de Privacidad establece los términos en que Dielthy usa y protege ' + 
      'la información que es proporcionada por sus usuarios al momento de utilizar su sitio web. ' + 
      'Esta compañía está comprometida con la seguridad de los datos de sus usuarios. Cuando le pedimos ' +
      'rellenar los campos de información personal con la cual usted pueda ser identificado, lo hacemos ' +
      'asegurando que sólo se empleará de acuerdo con los términos de este documento. Sin embargo esta ' +
      'Política de Privacidad puede cambiar con el tiempo o ser actualizada por lo que le recomendamos y ' +
      'enfatizamos revisar continuamente esta página para asegurarse que está de acuerdo con dichos cambios.',
    info: 'Información que es recogida',
    informationType: 'Nuestro sitio web podrá recoger información personal, como por ejemplo: nombre y apellidos, ' +
      'fecha de nacimiento, tipo de alimentación, información de contacto tal como su dirección de correo ' +
      'electrónico... Los datos relativos al nombre y apellidos, correo electrónico, fecha de nacimiento ' +
      'y tipo de alimentación serán públicos a toda persona registrada en la web.',
    informationUse: 'Uso de la información recogida',
    informationUseObjective: 'Nuestro sitio web emplea la información con el fin de proporcionar el mejor '+
      'servicio posible, particularmente para mantener un registro de usuarios, y mejorar nuestros productos '+
      'y servicios',
    compromise: 'Dielthy está altamente comprometido para cumplir con el compromiso de mantener su '+
      'información segura. Usamos los sistemas más avanzados y los actualizamos constantemente para '+
      'asegurarnos que no exista ningún acceso no autorizado.',
    links: 'Enlaces a Terceros',
    linksInfo: 'Este sitio web pudiera contener enlaces a otros sitios que pudieran ser de su interés. '+
      'Una vez que usted haga clic en estos enlaces y abandone nuestra página, ya no tenemos control sobre '+
      'el sitio al que es redirigido y por lo tanto no somos responsables de los términos o privacidad ni '+
      'de la protección de sus datos en dichos sitios. Estos están sujetos a sus propias políticas de '+
      'privacidad por lo cual es recomendable que los consulte para confirmar que usted está de acuerdo '+
      'con estas.',
    informationControlTitle: 'Control de su información personal',
    restrictInfo: 'En cualquier momento usted puede restringir la recopilación o el uso de la información '+
      'personal que es proporcionada a nuestro sitio web.',
    notSellInfo: 'Esta compañía no venderá, cederá ni distribuirá la información personal que es recopilada '+
      'sin su consentimiento, salvo que sea requerido por un juez con una orden judicial.',
    rightToChange: 'Dielthy se reserva el derecho de cambiar los términos de la presente Política de '+
      'Privacidad en cualquier momento.'
  },
  showProfile: {
    labels: {
      email: 'Email',
      password: 'Nueva contraseña',
      name: 'Nombre y apellidos',
      empty: 'Sin especificar',
      dietTypes: 'Tipo de dieta',
      birthDate: 'Fecha de nacimiento',
      password2: 'Confirmar contraseña',
      username: 'Nombre de usuario',
      edit: 'Editar',
      delete: 'Eliminar',
      dialogDelete: {
        question: '¿Deseas eliminar a este usuario?',
        confirm: 'Confirmar',
        cancel: 'Cancelar',
        reason: 'Motivo'
      }
    },
    buttons: {
      ok: 'OK',
      cancel: 'Cancelar',
      save: 'Guardar',
      notVerified: {
        info: 'Tu email no está validado.',
        link: 'Haz click para reenviar el correo de verificación'
      }
    },
    errors: {
      password: 'La contraseña debe tener al menos 6 caracteres',
      notEquals: 'Las contraseñas no coinciden',
      updated: 'Ha ocurrido un error actualizando la información',
      deleted: 'Ha ocurrido un error al eliminar al usuario',
      notFound: 'No se ha encontrado el usuario',
      reasonEmpty: 'El motivo no puede estar vacío'
    },
    emailSent: 'Se ha enviado el email correctamente',
    updated: 'Se ha actualizado la información',
    deleted: 'Usuario eliminado correctamente',
    deletedUser: 'Este usuario ha sido eliminado. Motivo: {reason}'
  },
  forum: {
    topic: {
      created: 'Creado el {date} por ',
      updated: 'Últ. act. {date}',
      tag: {
        veg: 'Vegano',
        cel: 'Celíaco',
        lac: 'Lactosa',
        none: 'General'
      }
    },
    noTopics: 'No se han encontrado temas',
    newTopic: 'Nuevo tema',
    filters: {
      title: 'Filtros',
      order: 'Ordenar por',
      name: 'Filtrar por nombre',
      filterBy: 'Filtrar por dieta',
      language: 'Idioma',
      orderItems: {
        updated: 'Última actualización',
        created: 'Fecha de creación'
      },
      myDiet: 'Mi dieta'
    },
    addTopic: {
      save: 'Añadir',
      close: 'Cerrar',
      tag: 'Elige una etiqueta',
      language: 'Elige un idioma',
      name: 'Nombre del tema',
      errors: {
        name: 'Debes especificar un nombre',
        tag: 'Debes especificar una etiqueta',
        notAdded: 'No se ha podido añadir el tema',
        language: 'Debes especificar un idioma'
      },
      success: 'Se ha añadido el tema correctamente'
    },
    comments: {
      created: 'Creado el {date}',
      commentButton: 'Comentar',
      replyButton: 'Responder',
      addCommentPlaceholder: 'Comentario',
      addReplyPlaceholder: 'Respuesta',
      error: 'El comentario no puede estar vacío',
      save: 'Publicar',
      close: 'Cerrar',
      noComments: 'No hay comentarios',
      reportButton: 'Denunciar',
      reportOk: 'Comentario denunciado con éxito',
      reportError: 'No se ha podido denunciar el comentario',
      dialogReport: {
        cancel: 'Cancelar',
        confirm: 'Confirmar',
        question: '¿Deseas denunciar este comentario?'
      },
      tooltipDelete: 'Eliminar',
      dialogDelete: {
        question: '¿Deseas eliminar este comentario?',
        cancel: 'Cancelar',
        confirm: 'Confirmar'
      },
      deleteOk: 'Se ha eliminado el comentario',
      deleteError: 'No se ha podido borrar el comentario'
    }
  },
  reports: {
    filters: {
      username: 'Filtrar por usuario'
    },
    noReports: 'No existen denuncias',
    numberOfReports: 'Denuncias: {num}',
    numberOfUsers: 'Usuarios que denunciaron: {num}',
    commentUser: 'Comentario de ',
    commentDate: 'Fecha del comentario: {date}',
    dialogDelete: {
      confirm: 'Confirmar',
      cancel: 'Cancelar',
      question: '¿Deseas eliminar este comentario?'
    },
    dialogDismiss: {
      confirm: 'Confirmar',
      cancel: 'Cancelar',
      question: '¿Deseas desestimar las denuncias de este comentario?'
    },
    errorDelete: 'No se ha podido borrar el comentario',
    errorDismiss: 'No se han podido desestimar las denuncias del comentario',
    deleted: 'Comentario borrado con éxito',
    dismissed: 'Denuncias desestimadas con éxito',
    tooltipDelete: 'Eliminar',
    tooltipDismiss: 'Desestimar'
  },
  license: 'Producto con licencia Creative Commons',
  language: {
    spanish: 'Español',
    english: 'English'
  }
};
