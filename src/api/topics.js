import { topicsCollection } from '../plugins/firebase';
import { getTodayString } from '../utils/sharedFunctions';

export default class TopicsService {

  loadTopics() {
    return topicsCollection.get();
  }

  newTopic(data) {
    const dataToSend = {
      nombre: data.name.trim(),
      tag: data.tag,
      idioma: data.language,
      mens: 0,
      creado: getTodayString(false),
      uidCreacion: data.uid,
      usuarioCreacion: data.user,
      actualizado: getTodayString(true)
    };
    const id = topicsCollection.doc().id;

    return topicsCollection.doc(id).set(dataToSend);
  }

  loadTopicData(idTopic) {
    return topicsCollection.doc(idTopic).get();
  }

  updateTopic(idTopic, messages) {
    const todayString = getTodayString(true);
    let dataToSend = {
      actualizado: todayString,
      mens: messages + 1
    };
    return topicsCollection.doc(idTopic).update(dataToSend);
  }
}