import { usersCollection, deletedUsersCollection } from '../plugins/firebase';

export default class UserService {
  getUserInfo(uid) {
    const docRef = usersCollection.doc(uid);
    return docRef.get();
  }
  deleteUserInfo(uid, reason) {
    return new Promise((resolve, reject) => {
      usersCollection.doc(uid).get().then(doc => {
        const data = doc.data();
        usersCollection.doc(uid).delete().then(() => {
          deletedUsersCollection.doc(uid).set({ ...data, motivo: reason }).then(() => {
            resolve();
          }).catch(() => {
            reject();
          });
        }).catch(() => {
          reject();
        });
      }).catch(() => {
        reject();
      });
    });
  }
  getDeletedUserInfo(uid) {
    return deletedUsersCollection.doc(uid).get();
  }
}