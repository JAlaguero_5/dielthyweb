import { commentsCollection, reportsCollection, topicsCollection } from '../plugins/firebase';
import firebase from 'firebase/app';
import { getTodayString } from '../utils/sharedFunctions';
import { customIndexOf } from '../utils/sharedFunctions';

export default class CommentsService {
  loadComments(idTopic) {
    return commentsCollection.doc(idTopic).collection('mensajes').get();
  }

  addComment(idTopic, data, idCommentToAnswer) {
    const todayString = getTodayString(true);
    let dataToSend = {
      comentario: data.comment,
      usuario: data.username,
      uidUsuario: data.uidUser,
      fecha: todayString,
      reportado: false
    };

    if (idCommentToAnswer) {
      return commentsCollection.doc(idTopic).collection('mensajes').doc(idCommentToAnswer).update({
        respuestas: firebase.firestore.FieldValue.arrayUnion(dataToSend)
      });
    } else {
      dataToSend.respuestas = [];
      return commentsCollection.doc(idTopic).collection('mensajes').doc().set(dataToSend);
    }
  }
  reportComment(idTopic, data, reportUserInfo) {
    const dataToSend = {
      ...data
    };
    delete dataToSend.isAnswer;
    delete dataToSend.idComentario;
    delete dataToSend.reportado;
    if (dataToSend.respuestas) {
      delete dataToSend.respuestas;
      dataToSend.esRespuesta = false;
    } else {
      dataToSend.esRespuesta = true;
    }
    const reportInfo = { ...reportUserInfo, reportes: 1 };

    return new Promise((resolve, reject) => {
      const id = data.id || data.idComentario;
      commentsCollection.doc(idTopic).collection('mensajes').doc(id).get().then(doc => {
        if (data.isAnswer) {
          // If it is an answer to a comment
          const answers = doc.data().respuestas;
          dataToSend.id = id;
          const customIndex = customIndexOf(answers, dataToSend);
          if (answers[customIndex].reportado) {
            reportsCollection.doc(answers[customIndex].idReporte).get().then(report => {
              if (report.data().usuariosReporte.map(item => item.idUsuarioReporte).includes(reportInfo.idUsuarioReporte)) {
                const totalReports = report.data().usuariosReporte;
                const reportUser = totalReports.find(item => item.idUsuarioReporte === reportInfo.idUsuarioReporte);
                reportUser.reportes++;
                reportsCollection.doc(answers[customIndex].idReporte).update({
                  reportes: firebase.firestore.FieldValue.increment(1),
                  usuariosReporte: totalReports
                }).then(() => {
                  resolve();
                }).catch(() => {
                  reject();
                });
              } else {
                reportsCollection.doc(answers[customIndex].idReporte).update({
                  reportes: firebase.firestore.FieldValue.increment(1),
                  usuariosReporte: firebase.firestore.FieldValue.arrayUnion({
                    ...reportInfo
                  })
                }).then(() => {
                  resolve();
                }).catch(() => {
                  reject();
                });
              }
            }).catch(() => {
              reject();
            });
          } else {
            const idReport = reportsCollection.doc().id;
            reportsCollection.doc(idReport).set({
              ...dataToSend,
              idTema: idTopic,
              reportes: 1,
              usuariosReporte: [{...reportInfo}],
              id
            }).then(() => {
              answers[customIndex].reportado = true;
              answers[customIndex].idReporte = idReport;
              commentsCollection.doc(idTopic).collection('mensajes').doc(id).update({
                respuestas: answers
              }).then(() => {
                resolve();
              }).catch(() => {
                reject();
              });
            }).catch(() => {
              reject();
            });
          }
        } else {
          // If it is not an answer to a comment
          if (doc.data().reportado) {
            reportsCollection.doc(doc.data().idReporte).get().then(report => {
              if (report.data().usuariosReporte.map(item => item.idUsuarioReporte).includes(reportInfo.idUsuarioReporte)) {
                const totalReports = report.data().usuariosReporte;
                const reportUser = totalReports.find(item => item.idUsuarioReporte === reportInfo.idUsuarioReporte);
                reportUser.reportes++;
                reportsCollection.doc(doc.data().idReporte).update({
                  reportes: firebase.firestore.FieldValue.increment(1),
                  usuariosReporte: totalReports
                }).then(() => {
                  resolve();
                }).catch(() => {
                  reject();
                });
              } else {
                reportsCollection.doc(doc.data().idReporte).update({
                  reportes: firebase.firestore.FieldValue.increment(1),
                  usuariosReporte: firebase.firestore.FieldValue.arrayUnion({
                    ...reportInfo
                  })
                }).then(() => {
                  resolve();
                }).catch(() => {
                  reject();
                });
              }
            }).catch(() => {
              reject();
            });
          } else {
            const idReport = reportsCollection.doc().id;
            reportsCollection.doc(idReport).set({
              reportes: 1,
              usuariosReporte: [{...reportInfo}],
              idTema: idTopic,
              ...dataToSend
            }).then(() => {
              commentsCollection.doc(idTopic).collection('mensajes').doc(id).update({
                reportado: true,
                idReporte: idReport
              }).then(() => {
                resolve();
              }).catch(() => {
                reject();
              });
            }).catch(() => {
              reject();
            });
          }
        }
      }).catch(() => {
        reject();
      });
    });
  }
  loadReports() {
    return reportsCollection.get();
  }
  deleteCommentFromReports(info) {
    return new Promise((resolve, reject) => {
      reportsCollection.doc(info.idReporte).delete().then(() => {
        if (info.esRespuesta) {
          commentsCollection.doc(info.idTema).collection('mensajes').doc(info.id).get().then(docInfo => {
            let answers = docInfo.data().respuestas;
            answers.splice(customIndexOf(answers, info), 1);
            commentsCollection.doc(info.idTema).collection('mensajes').doc(info.id).update({
              respuestas: answers
            }).then(() => {
              topicsCollection.doc(info.idTema).update({
                mens: firebase.firestore.FieldValue.increment(-1),
                actualizado: getTodayString(true)
              }).then(() => {
                resolve();
              }).catch(() => {
                reject();
              });
            }).catch(() => {
              reject();
            });
          })
          .catch(() => {
            reject();
          });
        } else {
          commentsCollection.doc(info.idTema).collection('mensajes').doc(info.id).get().then(docInfo => {
            let answers = docInfo.data().respuestas;
            if (answers.length) {
              const firstAnswer = answers.shift();
              commentsCollection.doc(info.idTema).collection('mensajes').doc(info.id).delete().then(() => {
                if (firstAnswer.reportado) {
                  const idToSend = commentsCollection.doc(info.idTema).collection('mensajes').doc().id;
                  reportsCollection.doc(firstAnswer.idReporte).update({
                    esRespuesta: false,
                    id: idToSend
                  }).then(() => {
                    commentsCollection.doc(info.idTema).collection('mensajes').doc(idToSend).set({
                      ...firstAnswer,
                      respuestas: answers
                    }).then(() => {
                      topicsCollection.doc(info.idTema).update({
                        mens: firebase.firestore.FieldValue.increment(-1),
                        actualizado: getTodayString(true)
                      }).then(() => {
                        resolve();
                      }).catch(() => {
                        reject();
                      });
                    }).catch(() => {
                      reject();
                    });
                  }).catch(() => {
                    reject();
                  });
                } else {
                  commentsCollection.doc(info.idTema).collection('mensajes').doc().set({
                    ...firstAnswer,
                    respuestas: answers
                  }).then(() => {
                    topicsCollection.doc(info.idTema).update({
                      mens: firebase.firestore.FieldValue.increment(-1),
                      actualizado: getTodayString(true)
                    }).then(() => {
                      resolve();
                    }).catch(() => {
                      reject();
                    });
                  }).catch(() => {
                    reject();
                  });
                }
              }).catch(() => {
                reject();
              });
            } else {
              commentsCollection.doc(info.idTema).collection('mensajes').doc(info.id).delete().then(() => {
                topicsCollection.doc(info.idTema).update({
                  mens: firebase.firestore.FieldValue.increment(-1),
                  actualizado: getTodayString(true)
                }).then(() => {
                  resolve();
                }).catch(() => {
                  reject();
                });
              }).catch(() => {
                reject();
              });
            }
          })
          .catch(() => {
            reject();
          });
        }
      })
      .catch(() => {
        reject();
      });
    });
  }
  deleteCommentFromTopic(info, idTopic) {
    return new Promise((resolve, reject) => {
      const idReporte = info.idReporte || null;
      if (info.respuestas !== undefined) {
        const answers = info.respuestas;
        commentsCollection.doc(idTopic).collection('mensajes').doc(info.id).delete().then(() => {
          if (answers.length) {
            const firstAnswer = answers.shift();
            if (firstAnswer.reportado) {
              const idToSend = commentsCollection.doc(idTopic).collection('mensajes').doc().id;
              reportsCollection.doc(firstAnswer.idReporte).update({
                esRespuesta: false,
                id: idToSend
              }).then(() => {
                commentsCollection.doc(idTopic).collection('mensajes').doc(idToSend).set({
                  ...firstAnswer,
                  respuestas: answers
                }).then(() => {
                  topicsCollection.doc(idTopic).update({
                    mens: firebase.firestore.FieldValue.increment(-1),
                    actualizado: getTodayString(true)
                  }).then(() => {
                    if (idReporte) {
                      reportsCollection.doc(idReporte).delete().then(() => {
                        resolve();
                      }).catch(() => {
                        reject();
                      });
                    } else {
                      resolve();
                    }
                  }).catch(() => {
                    reject();
                  });
                }).catch(() => {
                  reject();
                });
              }).catch(() => {
                reject();
              });
            } else {
              commentsCollection.doc(idTopic).collection('mensajes').doc().set({
                ...firstAnswer,
                respuestas: answers
              }).then(() => {
                topicsCollection.doc(idTopic).update({
                  mens: firebase.firestore.FieldValue.increment(-1),
                  actualizado: getTodayString(true)
                }).then(() => {
                  if (idReporte) {
                    reportsCollection.doc(idReporte).delete().then(() => {
                      resolve();
                    }).catch(() => {
                      reject();
                    });
                  } else {
                    resolve();
                  }
                }).catch(() => {
                  reject();
                });
              }).catch(() => {
                reject();
              });
            }
          } else {
            topicsCollection.doc(idTopic).update({
              mens: firebase.firestore.FieldValue.increment(-1),
              actualizado: getTodayString(true)
            }).then(() => {
              if (idReporte) {
                reportsCollection.doc(idReporte).delete().then(() => {
                  resolve();
                }).catch(() => {
                  reject();
                });
              } else {
                resolve();
              }
            }).catch(() => {
              reject();
            });
          }
        }).catch(() => {
          reject();
        });
      } else {
        commentsCollection.doc(idTopic).collection('mensajes').doc(info.id).get().then(docInfo => {
          const answers = docInfo.data().respuestas;
          answers.splice(customIndexOf(answers, info), 1);
          commentsCollection.doc(idTopic).collection('mensajes').doc(info.id).update({
            respuestas: answers
          }).then(() => {
            topicsCollection.doc(idTopic).update({
              mens: firebase.firestore.FieldValue.increment(-1),
              actualizado: getTodayString(true)
            }).then(() => {
              if (idReporte) {
                reportsCollection.doc(idReporte).delete().then(() => {
                  resolve();
                }).catch(() => {
                  reject();
                });
              } else {
                resolve();
              }
            }).catch(() => {
              reject();
            });
          }).catch(() => {
            reject();
          });
        });
      }
    });
  }
  dismissCommentReports(info) {
    const idTopic = info.idTema;
    const idComment = info.id;
    const idReport = info.idReporte;
    const isAnswer = info.esRespuesta;
    return new Promise((resolve, reject) => {
      if (!isAnswer) {
        reportsCollection.doc(idReport).delete().then(() => {
          commentsCollection.doc(idTopic).collection('mensajes').doc(idComment).update({
            idReporte: firebase.firestore.FieldValue.delete(),
            reportado: false
          }).then(() => {
            resolve();
          }).catch(() => {
            reject();
          });
        }).catch(() => {
          reject();
        });
      } else {
        commentsCollection.doc(idTopic).collection('mensajes').doc(idComment).get().then(doc => {
          let answers = doc.data().respuestas;
          const customIndex = customIndexOf(answers, info);
          answers[customIndex].reportado = false;
          delete answers[customIndex].idReporte;
          commentsCollection.doc(idTopic).collection('mensajes').doc(idComment).update({
            respuestas: answers
          }).then(() => {
            reportsCollection.doc(idReport).delete().then(() => {
              resolve();
            }).catch(() => {
              reject();
            });
          }).catch(() => {
            reject();
          });
        }).catch(() => {
          reject();
        });
      }
    });
  }
}
