import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth'
import config from './firebaseConfig';

firebase.initializeApp(config);

if (process.env.NODE_ENV !== 'test') {
  firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION);
}

const db = firebase.firestore();
const auth = firebase.auth();
const currentUser = auth.currentUser;

// Firebase collections
const aditiveCollection = db.collection('aditivos');
const usersCollection = db.collection('usuarios');
const topicsCollection = db.collection('temas');
const commentsCollection = db.collection('comentarios');
const reportsCollection = db.collection('denuncias');
const deletedUsersCollection = db.collection('eliminados');

export {
  db,
  auth,
  currentUser,
  aditiveCollection,
  usersCollection,
  topicsCollection,
  commentsCollection,
  reportsCollection,
  deletedUsersCollection
};