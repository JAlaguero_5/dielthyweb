import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify);

const opts = {
  theme: {
    themes: {
      light: {
        primary: '#03a9f4',
        secondary: '#ffc107',
        accent: '#795548',
        error: '#f44336',
        warning: '#ffeb3b',
        info: '#607d8b',
        success: '#8bc34a'
      }
    }
  }
};

export default new Vuetify(opts);