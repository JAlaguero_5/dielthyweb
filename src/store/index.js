import Vue from 'vue';
import Vuex from 'vuex';
import aditive from './modules/aditive';
import user from './modules/user';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    aditive,
    user
  }
});
