import { auth, usersCollection } from '../../plugins/firebase';

const state = {
  userInfo: {
    email: '',
    username: '',
    name: '',
    birthDate: null,
    dietTypes: [],
    uid: '',
    emailVerified: null,
    admin: null
  },
  registerForm: {
    email: '',
    password: '',
    password2: '',
    username: '',
    name: '',
    birthDate: null,
    dietTypes: []
  }
};

const actions = {
  createUser({ dispatch, commit }, data) {
    return new Promise((resolve, reject) => {
      auth.createUserWithEmailAndPassword(data.email, data.password)
      .then(res => {
        dispatch('sendEmail');
        commit('SET_EMAIL', res.user.email);
        return dispatch('saveUserInfo', data);
      })
      .then(() => {
        resolve();
      })
      .catch(() => {
        reject();
      });
    });
  },
  sendEmail() {
    auth.currentUser.sendEmailVerification();
  },
  saveUserInfo({ commit }, data) {
    return new Promise((resolve, reject) => {
      let dataToSend = {
        usuario: data.username,
        nombre: data.name,
        tiposDieta: data.dietTypes,
        email: data.email,
        admin: false
      };
      if (data.birthDate) {
        dataToSend.fechaNacimiento = data.birthDate;
      }
      usersCollection.doc(auth.currentUser.uid).set(dataToSend).then(() => {
        commit('SET_USERNAME', data.username);
        commit('SET_NAME', data.name);
        commit('SET_BIRTH_DATE', data.birthDate);
        commit('SET_DIET_TYPES', data.dietTypes);
        commit('SET_EMAIL', data.email);
        commit('SET_ADMIN', false);
        commit('SET_UID', auth.currentUser.uid);
        resolve();
      })
      .catch(() => {
        reject();
      });
    });
  },
  loadUserInfo({ commit }) {
    if (auth.currentUser) {
      return new Promise((resolve, reject) => {
        const docRef = usersCollection.doc(auth.currentUser.uid);
        docRef.get().then(document => {
          const data = document.data();
          commit('SET_USERNAME', data.usuario);
          commit('SET_NAME', data.nombre);
          commit('SET_BIRTH_DATE', data.fechaNacimiento);
          commit('SET_DIET_TYPES', data.tiposDieta);
          commit('SET_EMAIL', auth.currentUser.email);
          commit('SET_UID', auth.currentUser.uid);
          commit('SET_VERIFIED', auth.currentUser.emailVerified);
          commit('SET_ADMIN', data.admin);
          resolve(true);
        })
        .catch(() => {
          reject();
        });
      });
    } else {
      return new Promise(resolve => resolve(false));
    }
  },
  login({ dispatch }, data) {
    return new Promise((resolve, reject) => {
      auth.signInWithEmailAndPassword(data.email, data.password).then(() => {
        return dispatch('loadUserInfo');
      })
      .then(() => {
        resolve();
      })
      .catch(() => {
        auth.signOut();
        reject();
      });
    });
  },
  logout({ commit }) {
    return new Promise((resolve, reject) => {
      auth.signOut().then(() => {
        commit('CLEAR_USER');
        resolve();
      })
      .catch(() => {
        reject();
      });
    });
  },
  sendEmailResetPassword({ commit }, email) {
    return auth.sendPasswordResetEmail(email);
  },
  saveStateRegister({ commit }, data) {
    commit('SAVE_REGISTER_FORM', data);
  },
  clearRegisterForm({ commit }) {
    commit('CLEAR_REGISTER_FORM');
  },
  updateUserInfo({ commit }, data) {
    if (data.password) {
      return new Promise((resolve, reject) => {
        auth.currentUser.updatePassword(data.password).then(() => {
          let dataToSend = {
            usuario: data.username,
            nombre: data.name,
            tiposDieta: data.dietTypes.length === 0 ? ['none'] : data.dietTypes,
            fechaNacimiento: data.birthDate
          };
          return usersCollection.doc(auth.currentUser.uid).update(dataToSend);
        })
        .then(() => {
          commit('SET_USERNAME', data.username);
          commit('SET_NAME', data.name);
          commit('SET_BIRTH_DATE', data.birthDate);
          commit('SET_DIET_TYPES', data.dietTypes);
          resolve();
        })
        .catch(() => {
          reject();
        });
      });
    } else {
      return new Promise((resolve, reject) => {
        let dataToSend = {
          usuario: data.username,
          nombre: data.name,
          tiposDieta: data.dietTypes.length === 0 ? ['none'] : data.dietTypes,
          fechaNacimiento: data.birthDate
        };
        usersCollection.doc(auth.currentUser.uid).update(dataToSend).then(() => {
          commit('SET_USERNAME', data.username);
          commit('SET_NAME', data.name);
          commit('SET_BIRTH_DATE', data.birthDate);
          commit('SET_DIET_TYPES', data.dietTypes);
          resolve();
        })
        .catch(() => {
          reject();
        });
      });
    }
  }
};

const mutations = {
  SET_EMAIL(state, email) {
    state.userInfo.email = email;
  },
  SET_USERNAME(state, username) {
    state.userInfo.username = username;
  },
  SET_NAME(state, name) {
    state.userInfo.name = name;
  },
  SET_BIRTH_DATE(state, birthDate) {
    state.userInfo.birthDate = birthDate;
  },
  SET_DIET_TYPES(state, dietTypes) {
    state.userInfo.dietTypes = dietTypes;
  },
  SET_UID(state, uid) {
    state.userInfo.uid = uid;
  },
  SET_VERIFIED(state, verified) {
    state.userInfo.emailVerified = verified;
  },
  SET_ADMIN(state, admin) {
    sessionStorage.setItem('admin', admin);
    state.userInfo.admin = admin;
  },
  CLEAR_USER(state) {
    state.userInfo.email = '';
    state.userInfo.username = '';
    state.userInfo.name = '';
    state.userInfo.birthDate = null;
    state.userInfo.dietTypes = [];
    state.userInfo.uid = '';
    state.userInfo.admin = null;
    sessionStorage.removeItem('admin');
    state.userInfo.emailVerified = null;
  },
  SAVE_REGISTER_FORM(state, data) {
    state.registerForm.email = data.email;
    state.registerForm.username = data.username;
    state.registerForm.password = data.password;
    state.registerForm.password2 = data.password2;
    state.registerForm.name = data.name;
    state.registerForm.birthDate = data.birthDate;
    state.registerForm.dietTypes = data.dietTypes;
  },
  CLEAR_REGISTER_FORM(state) {
    state.registerForm.email = '';
    state.registerForm.password = '';
    state.registerForm.password2 = '';
    state.registerForm.username = '';
    state.registerForm.name = '';
    state.registerForm.birthDate = null;
    state.registerForm.dietTypes = [];
  }
};

export default {
  state,
  actions,
  mutations
};