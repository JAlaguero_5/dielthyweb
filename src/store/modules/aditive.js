import { aditiveCollection } from '../../plugins/firebase';

const state = {
  aditive: null
};

const actions = {
  loadAditiveInfo({ commit }, code) {
    return new Promise((resolve, reject) => {
      const aditiveRef = aditiveCollection.doc(code);
      aditiveRef.get().then(doc => {
        commit('SET_ADITIVE_INFO', doc.data());
        resolve();
      })
      .catch(() => {
        reject();
      });    
    });
  }
};

const mutations = {
  SET_ADITIVE_INFO(state, data) {
    state.aditive = {
      code: data.numero,
      type: data.clasificacion,
      description: data.descripcion,
      origin: data.origen,
      lac: data.lac,
      veg: data.veg,
      cel: data.cel
    };
  }
};

export default {
  state,
  actions,
  mutations
};
