export const getTodayString = withHour => {

  const today = new Date();
  let todayString = (`${today.getDate().toString().length > 1
      ? today.getDate().toString()
      : '0' + today.getDate()}`) +
    (`${(today.getMonth() + 1).toString().length > 1
      ? '-' + (today.getMonth() + 1).toString()
      : '-0' + (today.getMonth() + 1)}`) +
    `-${today.getFullYear().toString()}`;

  if (withHour) {
    todayString += ` ${today.getHours()}:` +
      `${today.getMinutes().toString().length > 1 ? today.getMinutes() : '0' + today.getMinutes()}`;
    return todayString;
  }
  return todayString;
};

export const getDate = (date, withHour) => {
  if (withHour) {
    const dateComplete = date.split(' ');
    const dateString = dateComplete[0].split('-');
    const hourString = dateComplete[1].split(':');
    return new Date(parseInt(dateString[2]), parseInt(dateString[1]) - 1, parseInt(dateString[0]),
      parseInt(hourString[0]), parseInt(hourString[1]));
  }
  return new Date(date.split('-')[2], date.split('-')[1] - 1, date.split('-')[0]);
};

export const customIndexOf = (arr, o) => {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].comentario === o.comentario && arr[i].fecha === o.fecha && arr[i].uidUsuario === o.uidUsuario &&
      arr[i].usuario === o.usuario) {
      return i;
    }
  }
  return -1;
};